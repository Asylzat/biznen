function Service(){
   this.serviceCounter = 0;
   this.serviceCount = 0;
   this.serviceCheckA = Array();


   $('.btn-check').click(function(){ return service.serviceCheck(); });
   $( '#sortable' ).sortable();

   this.reCount = function (obj = null){
      var sumAll = 0;

      if (obj != null){
         var scount = parseInt($(obj).parent().parent().find('.scount').val()), 
         sprize = parseFloat($(obj).parent().parent().find('.sprize').val()), 
         snds = parseInt($(obj).parent().parent().find('.snds').val()), 
         ssum = $(obj).parent().parent().find('.ssum'), 
         ssumShow = $(obj).parent().parent().find('.ssumShow');

         if (sprize > 0 && scount > 0){
            if (snds > 0) sprize = sprize + (sprize * snds) / 100;

            sumAll = sprize * scount;
            
            $(ssum).val(sumAll);
            $(ssumShow).html(sumAll);

            sumAll = 0;
         }
      }


      for (var j=1;j <= this.serviceCheckA.length;j++){
         if (this.serviceCheckA[j] !== undefined) {
            if ($(this.serviceCheckA[j]).find('.ssum').val() != '') sumAll += parseFloat($(this.serviceCheckA[j]).find('.ssum').val());
         }
      }

      $('.sumAll').html(sumAll);
      $('.sumAllInput').val(sumAll);

   }

   this.addService = function (number = '', id = '', title = '', prize = '0', count = '1', code = '796', code_name = 'шт', nds = '18', sum = ''){

      this.serviceCounter++;
      this.serviceCount++;
      var allServices = 
      "<div class='servId"+this.serviceCounter+" ui-state-default mylist2' onclick='service.refreshService()'> "+
         '<div class="col-lg-1 col-md-1 col-sm-1 serviceCounter">'+number+'</div>'+
         '<input name="Service['+this.serviceCounter+'][number]" type="hidden" class="serviceCounter2" value="'+number+'">'+
         '<input type="hidden" class="serviceId" name="Service['+this.serviceCounter+'][serviceId]" value="'+id+'">'+
         '<div class="col-lg-1 col-md-1 col-sm-1">'+
            '<span onclick="service.deleteService('+this.serviceCounter+')" class="glyphicon glyphicon-trash"></span> '+
            ' <span class="glyphicon glyphicon-fullscreen" style="float: right;"></span> '+
         '</div>'+
         '<div class="col-lg-5 col-md-5 col-sm-5">'+
            '<input name="Service['+this.serviceCounter+'][title]" class="form-controlG stitle" maxlength="1000" autofocus="" '+
            'placeholder="Наименование услуги / работы" type="text" value="'+title+'">'+
         '</div>'+
         '<div class="col-lg-1 col-md-1 col-sm-1">'+
            '<input name="Service['+this.serviceCounter+'][prize]" class="form-controlG sprize" maxlength="1000" autofocus="" '+
            'placeholder="0" type="text" onchange="service.reCount(this)" value="'+prize+'">'+
         '</div>'+
         '<div class="col-lg-1 col-md-1 col-sm-1">'+
            '<input name="Service['+this.serviceCounter+'][count]" class="form-controlG scount" maxlength="1000" autofocus="" '+
            'placeholder="1" type="text" onchange="service.reCount(this)" value="'+count+'">'+
         '</div>'+
         '<div class="col-lg-1 col-md-1 col-sm-1">'+
            '<input name="Service['+this.serviceCounter+'][code]" class="form-controlG scode" maxlength="1000" autofocus="" '+
            'placeholder="796" type="text" style="width:50%;float:left;" value="'+code+'">'+
            '<input name="Service['+this.serviceCounter+'][code_name]" class="form-controlG scode_name" maxlength="1000" autofocus="" '+
            'placeholder="шт" type="text" style="width:50%;float:left;" value="'+code_name+'">'+
         '</div>'+
         '<div class="col-lg-1 col-md-1 col-sm-1">'+
            '<input name="Service['+this.serviceCounter+'][nds]" class="form-controlG snds" maxlength="1000" autofocus="" '+
            'placeholder="18" type="text" onchange="service.reCount(this)" value="'+nds+'">'+
         '</div>'+
         '<div class="col-lg-1 col-md-1 col-sm-1">'+
            '<input name="Service['+this.serviceCounter+'][sum]" class="ssum" '+
            'type="hidden" value="'+sum+'"><span class="ssumShow">'+sum+'</span>'+
         '</div>'+
      "</div>";

      this.serviceCheckA[this.serviceCounter] = ".servId"+this.serviceCounter;
      $(".serviceList").append(allServices);
      this.refreshService();
      //alert("here we go");
   }

   this.refreshService = function (){
      for(var j = 1; j<=this.serviceCount;j++){
         $('.ui-state-default').filter(':nth-child('+j+')').find('.serviceCounter').html(j);
         $('.ui-state-default').filter(':nth-child('+j+')').find('.serviceCounter2').val(j);
      }
   }

   this.deleteService = function (i){
      if($('.servId'+i).find('.serviceId').val() !== 'undefined') 
         $('.ServiceDelete').val($('.ServiceDelete').val() + ';' + $('.servId'+i).find('.serviceId').val());
      $('.servId'+i).remove();
      this.serviceCount--;
      this.serviceCheckA[i] = '';
      this.refreshService();
   }

   this.serviceCheck = function (){
      var error = false;
      for (var j=1;j <= this.serviceCheckA.length;j++){
         if (this.serviceCheckA[j] !== undefined) {
            if (!validateVal($(this.serviceCheckA[j]).find('.stitle'), false, 2, 100)) error = true;
            if (!validateVal($(this.serviceCheckA[j]).find('.sprize'), true, 0, 100000)) error = true;
            if (!validateVal($(this.serviceCheckA[j]).find('.scount'), true, 1, 10000)) error = true;
            if (!validateVal($(this.serviceCheckA[j]).find('.scode'), false, 2, 5)) error = true;
            if (!validateVal($(this.serviceCheckA[j]).find('.scode_name'), false, 2, 20)) error = true;
            if (!validateVal($(this.serviceCheckA[j]).find('.snds'), true, 1, 100)) error = true;
         }
      }
      if (error) return false;
   }
}

var service = new Service();