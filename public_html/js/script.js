
function newValidateField(obj, field, min, max, email, param = 0){
   
   if ($('#optradio2').prop( "checked")) return validateField(obj, '#schet-email', 1, 30, true, 1);
    else return validateField(obj, '#schet-email', 0, 0, false);
}

function validateField(obj, field, min, max, email, param = 0)
      {
         var fieldText = $(field).val(), error = false, errorText = '';
         if (email){
            var atpos = fieldText.indexOf('@');
            var dotpos = fieldText.lastIndexOf('.');

            if (atpos < 1 || ( dotpos - atpos < 2 )) {
               error = true;
               errorText = ' Введите формат почты ';
            }
         }
         //console.log(fieldText);
         if (fieldText.length < min) {
            error = true;
            errorText = ' Мин - ' + min;
         }
         
         if (fieldText.length > max) {
            error = true;
            errorText = ' Max - ' + max;
         }

         if (error) 
         {
            $(field).parent().find('.help-block').html(errorText);
            $(field).parent().addClass('has-error');

            return false;
         } else $(field).parent().removeClass('has-error');

         switch(param){
            case 1 : $(obj).attr('href', $(obj).attr('href').replace('pdf', '').replace('excel', '') + fieldText); break;//schet
            case 2 : var withSchet = '', checkWith = $(field).parent().parent().find('.withSchet').find('input:checked');
               if(typeof $(checkWith).attr('docId') != 'undefined'){
                  withSchet = '?docId=' + $(checkWith).attr('docId');
                  fieldText = $(checkWith).attr('docType');
               }
               console.log($(checkWith).attr("docId"));
               $(obj).attr('href', '/' + fieldText + '/create'+withSchet);
               break;
         }
         //return false;
         return true;
      }

function validateVal(field, type, min, max)
      {
         var fieldText = field.val(), error = false, errorText = '';

         if (type){
            if (fieldText < min) {
               error = true;
               errorText = ' Мин - ' + min;
            }
            
            if (fieldText > max) {
               error = true;
               errorText = ' Max - ' + max;
            }
         } else {
            if (fieldText.length < min) {
               error = true;
               errorText = ' Мин - ' + min;
            }
            
            if (fieldText.length > max) {
               error = true;
               errorText = ' Max - ' + max;
            }            
         }


         if (error) 
         {
            field.parent().find('.help-block').html(errorText);
            field.parent().addClass('has-error');

            return false;
         } else $(field).parent().removeClass('has-error');

         return true;
      }


function selectRow(obj, controller){
   $(".mylist").css({ "background" : ""});
   $(".mylistIn").css({ "background" : ""});
   $(obj).css({ "background" : "#bff9f9"});

   if (controller == 'contragent'){   
      $(".edit").attr("href", "/" + controller + "/update?id=" + $(obj).find("div:first-child").html());
      $(".delete").attr("href", "/" + controller + "/delete?id=" + $(obj).find("div:first-child").html());
      $(".print").attr("href", "/" + controller + "/file?id=" + $(obj).find("div:first-child").html() + "&action=print")
      .attr('target', '_blank').attr('data-pjax', "0");
      $(".save").attr("href", "/" + controller + "/file?id=" + $(obj).find("div:first-child").html() + "&action=email&email=");
   }

   if (controller == 'document'){   
      var docType = $(obj).find(".docType").val();
      if (docType == 'schetFaktura') docType = 'schet-faktura';
      if (docType == 'platejPoruch') docType = 'platej-poruch';
      $(".edit").attr("href", "/" + docType + "/update?id=" + $(obj).find(".idself").val());
      $(".delete").attr("href", "/document/delete?id=" + $(obj).find(".id").val());
      
      $(".printBig").attr('data-whatever', "/" + docType + "/file?id="+$(obj).find(".idself").val());
      $(".saveBig").attr('data-whatever', "/" + docType + "/file?id="+$(obj).find(".idself").val());
      
      $(".createDoc").attr('data-whatever', $(obj).find(".docId").val());
   }

}




