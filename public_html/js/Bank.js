function Bank(){
   this.serviceCounter = 0;
   this.serviceCount = 0;
   this.serviceCheckA = Array();


   $('.btn').click(function(){ return bank.serviceCheck(); });
   //$('.linkBtn').click(function(){ return bank.serviceCheck(); });
   $( '#sortable' ).sortable();

   this.addService = function (number = '', id = '', bik = '', name = '', kor_schet = '', ras_schet = ''){

      this.serviceCounter++;
      this.serviceCount++;
      var allServices = 
      "<div class='servId"+this.serviceCounter+" ui-state-default mylist2' onclick='bank.refreshService()'> "+
         '<div class="col-lg-1 col-md-1 col-sm-1 serviceCounter">'+number+'</div>'+
         '<input name="ContagentBank['+this.serviceCounter+'][number]" type="hidden" class="serviceCounter2" value="'+number+'">'+
         '<input type="hidden" class="serviceId" name="ContagentBank['+this.serviceCounter+'][serviceId]" value="'+id+'">'+
         '<div class="col-lg-1 col-md-1 col-sm-1">'+
            '<span onclick="bank.deleteService('+this.serviceCounter+')" class="glyphicon glyphicon-trash"></span> '+
            ' <span class="glyphicon glyphicon-fullscreen" style="float: right;"></span> '+
         '</div>'+
         '<div class="col-lg-2 col-md-2 col-sm-4">'+
            '<input name="ContagentBank['+this.serviceCounter+'][bik]" class="form-controlG sbik" maxlength="1000" autofocus="" '+
            'placeholder="БИК" type="text" value="'+bik+'">'+
         '</div>'+
         '<div class="col-lg-3 col-md-3 col-sm-6">'+
            '<input name="ContagentBank['+this.serviceCounter+'][name]" class="form-controlG sname" maxlength="1000" autofocus="" '+
            'placeholder="Наименование Банка и город" type="text" value="'+name+'">'+
         '</div>'+
         '<div class="col-lg-3 col-md-3 col-sm-7">'+
            '<input name="ContagentBank['+this.serviceCounter+'][kor_schet]" class="form-controlG skor_schet" maxlength="1000" autofocus="" '+
            'placeholder="Кор. счет (к/с)" type="text" value="'+kor_schet+'">'+
         '</div>'+
         '<div class="col-lg-2 col-md-2 col-sm-5">'+
            '<input name="ContagentBank['+this.serviceCounter+'][ras_schet]" class="form-controlG sras_schet" maxlength="1000" autofocus="" '+
            'placeholder="Расчетный счет (р/с)" type="text" value="'+ras_schet+'">'+
         '</div>'+
      "</div>";

      this.serviceCheckA[this.serviceCounter] = ".servId"+this.serviceCounter;
      $("#sortable").append(allServices);
      this.refreshService();
      //alert("here we go");
   }

   this.refreshService = function (){
      for(var j = 1; j<=this.serviceCount;j++){
         $('#sortable .ui-state-default').filter(':nth-child('+j+')').find('.serviceCounter').html(j);
         $('#sortable .ui-state-default').filter(':nth-child('+j+')').find('.serviceCounter2').val(j);
      }
   }

   this.deleteService = function (i){
      if($('#sortable .servId'+i).find('.serviceId').val() !== 'undefined') 
         $('.ServiceDelete').val($('.ServiceDelete').val() + ';' + $('#sortable .servId'+i).find('.serviceId').val());
      $('#sortable .servId'+i).remove();
      this.serviceCount--;
      this.serviceCheckA[i] = '';
      this.refreshService();
   }

   this.serviceCheck = function (){
      var error = false;
      for (var j=1;j <= this.serviceCheckA.length;j++){
         if (this.serviceCheckA[j] !== undefined) {
            if (!validateVal($(this.serviceCheckA[j]).find('.sbik'), false, 1, 100)) error = true;
            if (!validateVal($(this.serviceCheckA[j]).find('.sname'), false, 1, 200)) error = true;
            if (!validateVal($(this.serviceCheckA[j]).find('.skor_schet'), false, 1, 200)) error = true;
            if (!validateVal($(this.serviceCheckA[j]).find('.sras_schet'), false, 1, 200)) error = true;
         }
      }
      if (error) return false;
   }
}

var bank = new Bank();




function ContactInform(){
   this.serviceCounter = 0;
   this.serviceCount = 0;
   this.serviceCheckA = Array();


   $('.btn').click(function(){ return contactInform.serviceCheck(); });
   //$('.linkBtn').click(function(){ return contactInform.serviceCheck(); });
   $( '#sortable-inform' ).sortable();

   this.addService = function (number = '', id = '', fio = '', email = '', inform = ''){

      this.serviceCounter++;
      this.serviceCount++;
      var allServices = 
      "<div class='servId"+this.serviceCounter+" ui-state-default mylist2' onclick='contactInform.refreshService()'> "+
         '<div class="col-lg-1 col-md-1 col-sm-1 serviceCounter">'+number+'</div>'+
         '<input name="ContactInform['+this.serviceCounter+'][number]" type="hidden" class="serviceCounter2" value="'+number+'">'+
         '<input type="hidden" class="serviceId" name="ContactInform['+this.serviceCounter+'][serviceId]" value="'+id+'">'+
         '<div class="col-lg-1 col-md-1 col-sm-1">'+
            '<span onclick="contactInform.deleteService('+this.serviceCounter+')" class="glyphicon glyphicon-trash"></span> '+
            ' <span class="glyphicon glyphicon-fullscreen" style="float: right;"></span> '+
         '</div>'+
         '<div class="col-lg-3 col-md-3 col-sm-8">'+
            '<input name="ContactInform['+this.serviceCounter+'][fio]" class="form-controlG sfio" maxlength="1000" autofocus="" '+
            'placeholder="ФИО сотрудника" type="text" value="'+fio+'">'+
         '</div>'+
         '<div class="col-lg-3 col-md-3 col-sm-5">'+
            '<input name="ContactInform['+this.serviceCounter+'][email]" class="form-controlG semail" maxlength="1000" autofocus="" '+
            'placeholder="E-Mail" type="text" value="'+email+'">'+
         '</div>'+
         '<div class="col-lg-4 col-md-4 col-sm-7">'+
            '<input name="ContactInform['+this.serviceCounter+'][inform]" class="form-controlG sinform" maxlength="1000" autofocus="" '+
            'placeholder="Дополнительная информация" type="text" value="'+inform+'">'+
         '</div>'+
      "</div>";

      this.serviceCheckA[this.serviceCounter] = ".servId"+this.serviceCounter;
      $("#sortable-inform").append(allServices);
      this.refreshService();
      //alert("here we go");
   }

   this.refreshService = function (){
      for(var j = 1; j<=this.serviceCount;j++){
         $('#sortable-inform .ui-state-default').filter(':nth-child('+j+')').find('.serviceCounter').html(j);
         $('#sortable-inform .ui-state-default').filter(':nth-child('+j+')').find('.serviceCounter2').val(j);
      }
   }

   this.deleteService = function (i){
      if($('#sortable-inform .servId'+i).find('.serviceId').val() !== 'undefined') 
         $('.ServiceDelete2').val($('.ServiceDelete2').val() + ';' + $('#sortable-inform .servId'+i).find('.serviceId').val());
      $('#sortable-inform .servId'+i).remove();
      this.serviceCount--;
      this.serviceCheckA[i] = '';
      this.refreshService();
   }

   this.serviceCheck = function (){
      var error = false;
      for (var j=1;j <= this.serviceCheckA.length;j++){
         if (this.serviceCheckA[j] !== undefined) {
            if (!validateVal($(this.serviceCheckA[j]).find('.sfio'), false, 1, 100)) error = true;
            if (!validateVal($(this.serviceCheckA[j]).find('.semail'), false, 1, 200)) error = true;
            if (!validateVal($(this.serviceCheckA[j]).find('.sinform'), false, 1, 200)) error = true;
         }
      }
      if (error) return false;
   }
}

var contactInform = new ContactInform();




