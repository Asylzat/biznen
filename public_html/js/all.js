$(function () {

	// One string example
	(function () {
		var $container = $('.field-signupform-ur_adress');

		var $address = $container.find('[name="SignupForm[ur_adress]"]');

		$address.kladr({
			oneString: true,
			change: function (obj) {
				log(obj);
			}
		});

		function log(obj) {
			var $log, i;

			$container.find('.js-log li').hide();

			for (i in obj) {
				$log = $container.find('[data-prop="' + i + '"]');

				if ($log.length) {
					$log.find('.value').text(obj[i]);
					$log.show();
				}
			}
		}
	})();

	// One string example
	(function () {
		var $container = $('.field-signupform-fak_adress');

		var $address = $container.find('[name="SignupForm[fak_adress]"]');

		$address.kladr({
			oneString: true,
			change: function (obj) {
				log(obj);
			}
		});

		function log(obj) {
			var $log, i;

			$container.find('.js-log li').hide();

			for (i in obj) {
				$log = $container.find('[data-prop="' + i + '"]');

				if ($log.length) {
					$log.find('.value').text(obj[i]);
					$log.show();
				}
			}
		}
	})();	
	
});
