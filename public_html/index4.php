<?php
session_start();

$arrData = '';

?>

<!doctype html>
<html lang="ru-RU">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Новинки раскрасок и картин по номерам</title>
</head>

<body>

<?php
echo $_SESSION["userId"];
echo '<br>' . $_SESSION["access_token"];
class getInstagram {
  public function media(){
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL, "https://api.instagram.com/v1/users/" . $_SESSION["userId"] . "/media/recent?access_token=" . $_SESSION["access_token"]);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 0);

    $json = curl_exec($ch);

    curl_close($ch);

    $json = json_decode($json, true);

    $result = '';

    foreach ($json['data'] as $current){

      $result .= "<a href=" . $current['link'] . "><img src=" . $current['images']['standard_resolution']['url'] . " width=" . $current['images']['standard_resolution']['width'] . 
        " height=" . $current['images']['standard_resolution']['height'] . " alt=" . $current['caption']['text'] . "><p>" . $current['caption']['text'] . "</p></a>";
    }

    return $result;
  }

  public function sessionCreate(){
    if (isset($_GET['code'])) {

      $fields = array(
             'client_id'     => '6d388cb3551e4bc38aaca6d274462e81',
             'client_secret' => 'b8fd4e3bd3ce4391897f63a0cfb6ebb5',
             'grant_type'    => 'authorization_code',
             'redirect_uri'  => 'http://asylza.beget.tech/index4.php',
             'code'          => $_GET['code']
      );
      $url = 'https://api.instagram.com/oauth/access_token';
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, 20);
      curl_setopt($ch,CURLOPT_POST,true); 
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);


      $json = curl_exec($ch);
      curl_close($ch);

      $json = json_decode($json, true);

      if (!isset($json["access_token"])){
        var_dump($fields);
        echo "<p>oauth error</p>";
        var_dump($json);
      } else {

        var_dump($json);

        $_SESSION['userId'] = $json["user"]["id"];
        $_SESSION['access_token'] = $json["access_token"];

      }

    } else {
      echo "<p><a href='https://www.instagram.com/oauth/authorize/?client_id=6d388cb3551e4bc38aaca6d274462e81&redirect_uri=http://asylza.beget.tech/index4.php&response_type=code'>
        Необходимо получить токен</a></p>";
    }
  }
}

$instagram = new getInstagram();

if (isset($_SESSION['access_token'])) {

  echo $instagram->media();

} else {
  $instagram->sessionCreate();
}

?>

</body>

</html>