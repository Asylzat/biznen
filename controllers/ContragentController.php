<?php

namespace app\controllers;

use Yii;
use app\models\Contragent;
use app\models\ContragentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\models\Schet;

/**
 * ContragentController implements the CRUD actions for Contragent model.
 */
class ContragentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view', 'delete', 'create', 'profile', 'balance', 'podpis', 'start', 'file'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['index', 'update', 'view', 'delete', 'create', 'profile', 'balance', 'podpis', 'start', 'file'],
                        'allow' => true,
                        'roles' => ['author'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contragent models.
     * @return mixed
     */

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {   
        $searchModel = new ContragentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contragent model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contragent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contragent();
        $model->createdBy = Yii::$app->user->getId();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            \app\models\ContagentBank::serviceData(Yii::$app->request->post(), $model->id, 'contragent_id');
            \app\models\ContactInform::serviceData(Yii::$app->request->post(), $model->id, 'contragent_id');
            
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Contragent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \app\models\ContagentBank::serviceData(Yii::$app->request->post(), $model->id, 'contragent_id');
            \app\models\ContactInform::serviceData(Yii::$app->request->post(), $model->id, 'contragent_id');
            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionProfile()
    {
        
        return $this->render('profile', [
        ]);
    }

    public function actionBalance()
    {
        return $this->render('balance');
    }

    public function actionFile($id, $action, $email = null, $doctype = null)
    {

        $model = $this->findModel($id);
        if ($model != null) {

          if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['author'])) 
            if ($model->createdBy == Yii::$app->user->getId()) {
              error_reporting(E_ALL);
              ini_set('display_errors', TRUE);
              ini_set('display_startup_errors', TRUE);


              $fileHelper = new Schet();
              $fileHelper->pathPHPExcel = str_replace('/public_html', '', $_SERVER['DOCUMENT_ROOT']) . '/doc_create';
              require_once $fileHelper->pathPHPExcel . '/Classes/PHPExcel.php';

              $objPHPExcel = new \PHPExcel();

              $objPHPExcel->getProperties()->setCreator("Biznen.com")
                                           ->setLastModifiedBy("Biznen.com")
                                           ->setTitle("schet" . $id)
                                           ->setSubject("Office 2007 XLSX schet" . $id)
                                           ->setDescription("Biznen create easy.")
                                           ->setKeywords("office 2007 openxml php")
                                           ->setCategory("schet");

              $styleArray = array(
                  'borders' => array(
                      'outline' => array(
                          'style' => \PHPExcel_Style_Border::BORDER_THICK,
                          'color' => array('argb' => 'FFFF0000'),
                      ),
                  ),
              );

              $objPHPExcel->getActiveSheet()->getStyle('A2:L8')->applyFromArray($styleArray);
              $objPHPExcel->getActiveSheet()->mergeCells('B2:T3');
              $objPHPExcel->getActiveSheet()->mergeCells('U2:Y2');
              $objPHPExcel->getActiveSheet()->mergeCells('Z2:AM2');
              $objPHPExcel->getActiveSheet()->mergeCells('B4:T4');

              $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(1);

              $objPHPExcel->setActiveSheetIndex(0)
                          ->setCellValue('B2', 'ВТБ 24 (ПАО) Г. МОСКВА')
                          ->setCellValue('U2', 'БИК')
                          ->setCellValue('Z2', '044525716')
                          ->setCellValue('B4', 'Банк получателя !')
                          ->setCellValue('B5', 'ИНН');

              $objPHPExcel->getActiveSheet()->setTitle('Simple');

              $objPHPExcel->setActiveSheetIndex(0);

              if ($action == 'save' && $doctype == 'excel') $fileHelper->saveExcelFile($objPHPExcel, 'attachment');

              else if (($action == 'save' && $doctype == 'pdf') || $action == 'print'){
                if ($action == 'save') $fileHelper->savePDFFile($objPHPExcel, 'attachment');
                else if ($action == 'print') $fileHelper->savePDFFile($objPHPExcel, 'inline');
              }
                


           if ($action == 'email'){
                if ($email == null) $email = Yii::$app->user->identity->email;
              $fileHelper->sendEmail($objPHPExcel, $email);
              return $this->redirect(['index']);
          }
        } else throw new NotFoundHttpException('Этот документ вам не принадлежит.');
      } else throw new NotFoundHttpException('Произошла ошибка свяжитесь с администратором.');
    }

    public function actionStart()
    {

        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);


        $doc_create = str_replace('/public_html', '', $_SERVER['DOCUMENT_ROOT']) . '/doc_create';
        require_once $doc_create . '/Classes/PHPExcel.php';


        $rendererName = \PHPExcel_Settings::PDF_RENDERER_MPDF;
        $rendererLibrary = 'MPDF57';

        $rendererLibraryPath = $doc_create .'/MPDF57';

        $objPHPExcel = new \PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("PDF Test Document")
                                     ->setSubject("PDF Test Document")
                                     ->setDescription("Test document for PDF, generated using PHP classes.")
                                     ->setKeywords("pdf php")
                                     ->setCategory("Test result file");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Hello')
                    ->setCellValue('B2', 'world!')
                    ->setCellValue('C1', 'Hello')
                    ->setCellValue('D2', 'world!');

        // Miscellaneous glyphs, UTF-8
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A4', 'Miscellaneous glyphs')
                    ->setCellValue('A5', 'Выполненно');

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Simple');
        $objPHPExcel->getActiveSheet()->setShowGridLines(false);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        if (!\PHPExcel_Settings::setPdfRenderer(
                $rendererName,
                $rendererLibraryPath
            )) {
            die(
                'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
                '<br />' .
                'at the top of this script as appropriate for your directory structure'
            );
        }


        // Redirect output to a client’s web browser (PDF)
        header('Content-Type: application/pdf');
        //header('Content-Disposition: attachment;filename="01simple.pdf"');
        header('Content-Disposition: inline;filename="01simple.pdf"');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
        $objWriter->save('php://output');
        exit;


    }


    public function actionPodpis()
    {
        return $this->render('podpis');
    }

    /**
     * Deletes an existing Contragent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->banks !== null) {
          foreach ($model->banks as $current) {
            $current->delete();
          }
        }        

        if ($model->informs !== null) {
          foreach ($model->informs as $current) {
            $current->delete();
          }
        }        

        if ($model->pokupatel !== null) {
          foreach ($model->pokupatel as $current) {
            $current->delete();
          }
        }        


        if ($model->prodaves !== null) {
          foreach ($model->prodaves as $current) {
            $current->delete();
          }
        }        

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Contragent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contragent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contragent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
