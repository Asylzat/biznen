<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SendMail;

use app\models\SignupForm;
use app\models\User;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {   
       /*$auth = Yii::$app->authManager;

        // add "createPost" permission
        $createPost = $auth->createPermission('createPost');
        $createPost->description = 'Create a post';
        $auth->add($createPost);

        // add "updatePost" permission
        $updatePost = $auth->createPermission('updatePost');
        $updatePost->description = 'Update post';
        $auth->add($updatePost);

        // add "author" role and give this role the "createPost" permission
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createPost);

        // add the rule
        $rule = new \app\rbac\AuthorRule;
        $auth->add($rule);

        // add the "updateOwnPost" permission and associate the rule with it.
        $updateOwnPost = $auth->createPermission('updateOwnPost');
        $updateOwnPost->description = 'Update own post';
        $updateOwnPost->ruleName = $rule->name;
        $auth->add($updateOwnPost);

        // "updateOwnPost" will be used from "updatePost"
        $auth->addChild($updateOwnPost, $updatePost);

        // allow "author" to update their own posts
        $auth->addChild($author, $updateOwnPost);


        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updatePost);
        $auth->addChild($admin, $author);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($author, 2);
        $auth->assign($admin, 1);
        */


        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/document');//return $this->goBack();
        }

        $registered = false;

        $model2 = new SignupForm();
        if ($model2->load(Yii::$app->request->post()) && $model2->uniqUsername() && $model2->uniqEmail()
            ) {
            $user = new User();
            $user->username = $model2->username;

            $user->name_contr = $model2->name_contr;
            $user->kpp = $model2->kpp;
            $user->ogrn = $model2->ogrn;
            $user->gen_director = $model2->gen_director;
            $user->phone = $model2->phone;
            $user->ur_adress = $model2->ur_adress;
            $user->fak_adress = $model2->fak_adress;
          
            $user->email = $model2->email;
            $user->money = 10;
            $password = rand(1,1000) . rand(1,1000) . rand(1,1000) . rand(1,1000);
            $user->password = $user->setPassword($password);
            $user->save(false);

            $auth = Yii::$app->authManager;
            $authorRole = $auth->getRole('author');
            $auth->assign($authorRole, $user->getId());

            $contact = new SendMail();
            $contact->name = 'Biznen';
            $contact->email = Yii::$app->params['adminEmail'];
            $contact->subject = 'Вы создали учетную запись на Biznen.com!';
            $contact->body = \Yii::$app->view->renderFile('@app/views/user/_signupMail.php', [
                'password' => $password, 
                ]);
            $registered = true;
            $contact->contact($model2->email);

            //return $user;
            return $this->render('login', [
                'model' => $model,
                'model2' => $model2,
                'registered' => $registered,
            ]);
        }

        return $this->render('login', [
            'model' => $model,
            'model2' => $model2,
            'registered' => false,
        ]);
    }

    public function actionMess(){
        $contact = new SendMail();
        $contact->name = 'lqwerty';
        $contact->email = 'my@mail.ru';
        $contact->subject = 'from site';
        $contact->body = 'your password is';
        var_dump($contact->contact(Yii::$app->params['adminEmail']));

    } 

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionHowit()
    {
        return $this->render('how_it');
    }

}
