<?php

namespace app\controllers;

use Yii;
use app\models\Act;
use app\models\ActSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\models\Schet;

/**
 * ActController implements the CRUD actions for Act model.
 */
class ActController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view', 'delete', 'create', 'file'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['index', 'update', 'view', 'file', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['author'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Act models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ActSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Act model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Torg12 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($docId = null)
    {
        $model = new Act();

        if ($docId != null && ($document = \app\models\Document::findOne($docId)) !== null) {

            if ($document->createdBy != Yii::$app->user->getId()) 
                throw new NotFoundHttpException('Документ создан не вами.' . $document->createdBy);
        } else {
            $docId = null;
            $document = new \app\models\Document();
        }
        if ($model->load(Yii::$app->request->post())) {

            $model->createdBy = Yii::$app->user->getId();
            if ($docId != null) {

                $model->id_document = $document->id;
                if ($model->save(false)){
                    \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'act_id');
                    return $this->redirect('/document');
                } else throw new NotFoundHttpException('Не удалось сохранить акт.');
            } else {
                $document->load(Yii::$app->request->post());
                $document->createdBy = Yii::$app->user->getId();
                if ($document->save(false)){
                    $model->id_document = $document->id;
                    if ($model->save(false)){
                        \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'act_id');
                        return $this->redirect(['/document']);
                    } else throw new NotFoundHttpException('Не удалось сохранить акт.');
                } else throw new NotFoundHttpException('Не смог создать документ, обратитесь к администратору.');
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'document' => $document,
                'docId' => $docId,
            ]);
        }
    }
    
    /**
     * Updates an existing Torg12 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $document = $model->document;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $document->load(Yii::$app->request->post());

            if ($model->idnum == '') $model->idnum = $model->id;

            $document->save(false);
            
            \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'act_id');

            return $this->render('update', [
                'model' => $model,
                'document' => $document,
                'docId' => null,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'document' => $document,
                'docId' => null,
            ]);
        }
    }

    /**
     * Deletes an existing Act model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->document !== null) if ($model->document->schet === null) $model->document->delete();
        if ($model->services !== null) {
          foreach ($model->services as $current) {
            $current->delete();
          }
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    public function actionFile($id, $action, $email = null, $doctype = null)
    {

        $model = $this->findModel($id);
        if ($model != null) {

          if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['author'])) 
            if ($model->createdBy == Yii::$app->user->getId()) {
              error_reporting(E_ALL);
              ini_set('display_errors', TRUE);
              ini_set('display_startup_errors', TRUE);


              $fileHelper = new Schet();
              $fileHelper->pathPHPExcel = str_replace('/public_html', '', $_SERVER['DOCUMENT_ROOT']) . '/doc_create';
              require_once $fileHelper->pathPHPExcel . '/Classes/PHPExcel.php';


              $rowAdded = 0;

              /*********************************************************************************************************************/
              /**************************************    Editable data for schet excel    ******************************************/
              /*********************************************************************************************************************/

              $objPHPExcel= \PHPExcel_IOFactory::load($fileHelper->pathPHPExcel . '/template/actv.xls');

             $objPHPExcel->setActiveSheetIndex(0);
              $objWorksheet = $objPHPExcel->getActiveSheet();
              $objWorksheet->getStyle('A1:AN100')->applyFromArray($fileHelper->style()['clear']);



            /*$counter = 0;
            foreach($model->document->prodaves->banks as $current){
              if ($counter > 0){
                $objWorksheet->insertNewRowBefore(5 + $rowAdded, 3);
                $rowAdded += 3;
              }

              $objPHPExcel->getActiveSheet()->setCellValue('B' . (2 + $rowAdded), $current->name); 
              $objPHPExcel->getActiveSheet()->setCellValue('Z' . (2 + $rowAdded), $current->bik); 
              $objPHPExcel->getActiveSheet()->setCellValue('Z' . (3 + $rowAdded), $current->kor_schet);
              $objPHPExcel->getActiveSheet()->setCellValue('B'.(4 + $rowAdded), 'Банк получателя');
              $objPHPExcel->getActiveSheet()->setCellValue('U'.(2 + $rowAdded), 'БИК');
              $objPHPExcel->getActiveSheet()->setCellValue('U'.(3 + $rowAdded), 'Сч. №');

              $objPHPExcel->getActiveSheet()->mergeCells('B'.(2 + $rowAdded).':T'.(3 + $rowAdded));
              $objPHPExcel->getActiveSheet()->mergeCells('B'.(4 + $rowAdded).':T'.(4 + $rowAdded));
              $objPHPExcel->getActiveSheet()->mergeCells('U'.(2 + $rowAdded).':Y'.(2 + $rowAdded));
              $objPHPExcel->getActiveSheet()->mergeCells('U'.(3 + $rowAdded).':Y'.(4 + $rowAdded));
              $objPHPExcel->getActiveSheet()->mergeCells('Z'.(2 + $rowAdded).':AM'.(2 + $rowAdded));
              $objPHPExcel->getActiveSheet()->mergeCells('Z'.(3 + $rowAdded).':AM'.(4 + $rowAdded));

              $objPHPExcel->getActiveSheet()->getStyle('B'.(4 + $rowAdded).':T'.(4 + $rowAdded))->applyFromArray($fileHelper->style()['borderTopNone']);
              $objPHPExcel->getActiveSheet()->getStyle('B'.(4 + $rowAdded).':T'.(4 + $rowAdded))->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
              $objPHPExcel->getActiveSheet()->getStyle('Z'.(3 + $rowAdded).':AM'.(4 + $rowAdded))->applyFromArray($fileHelper->style()['borderTopNone']);


              $counter++;            
            }

            /*$objPHPExcel->getActiveSheet()->getStyle('A1');
            $objPHPExcel->getActiveSheet()->duplicateStyle(
              $objPHPExcel->getActiveSheet()->g‌etStyle('A1'),
              'A2:A10'); */

            $objWorksheet->setCellValue('B' . (3 + $rowAdded), 'Акт № ' . $model->id . ' от ' . $model->date . ' г.')
                          ->setCellValue('F' . (5 + $rowAdded), $model->document->prodaves->name_contr)
                          ->setCellValue('F' . (7 + $rowAdded), $model->document->pokupatel->name_contr);

              /*********************************************************************************************************************/
              /**************************************    schet service    **********************************************************/

            $counter = 0;
            foreach($model->document->schet->services as $current){
              
              if ($counter > 0){
                $rowAdded++;
                $objWorksheet->insertNewRowBefore(11 + $rowAdded, 1);
              }

              $objWorksheet->mergeCells('B'.(11 + $rowAdded).':C'.(11 + $rowAdded));
              $objWorksheet->mergeCells('D'.(11 + $rowAdded).':T'.(11 + $rowAdded));
              $objWorksheet->mergeCells('U'.(11 + $rowAdded).':W'.(11 + $rowAdded));
              $objWorksheet->mergeCells('X'.(11 + $rowAdded).':Y'.(11 + $rowAdded));
              $objWorksheet->mergeCells('Z'.(11 + $rowAdded).':AC'.(11 + $rowAdded));
              $objWorksheet->mergeCells('AD'.(11 + $rowAdded).':AG'.(11 + $rowAdded));

              $objWorksheet->setCellValue('B'.(11 + $rowAdded), $current->number)
              ->setCellValue('D' . (11 + $rowAdded), $current->title)
              ->setCellValue('U' . (11 + $rowAdded), $current->count) 
              ->setCellValue('X' . (11 + $rowAdded), $current->code_name)              
              ->setCellValue('Z' . (11 + $rowAdded), $current->prize)           
              ->setCellValue('AD' . (11 + $rowAdded), $current->sum);              

              //$objPHPExcel->getActiveSheet()->getStyle('B'.(11 + $rowAdded).':AN'.(11 + $rowAdded))->applyFromArray($fileHelper->style()['fontNormal']);
              $objWorksheet->getStyle('D'.(11 + $rowAdded).':T'.(11 + $rowAdded))->applyFromArray($fileHelper->style()['fontLeft']);
              $objWorksheet->getStyle('U'.(11 + $rowAdded).':W'.(11 + $rowAdded))->applyFromArray($fileHelper->style()['fontRight']);
              $objWorksheet->getStyle('Z'.(11 + $rowAdded).':AG'.(11 + $rowAdded))->applyFromArray($fileHelper->style()['fontRight']);

              $counter++;

            }

              $objWorksheet->setCellValue('AD' . (17 + $rowAdded), $model->document->schet->sum)           
              ->setCellValue('B' . (18 + $rowAdded), 'Всего оказано услуг '.$counter.', на сумму '.$model->document->schet->sum.' руб.')           

              ->setCellValue('B'.(24 + $rowAdded), $model->document->prodaves->gen_director)
              ->setCellValue('U'.(24 + $rowAdded), $model->document->pokupatel->gen_director);



              /*********************************************************************************************************************/
              /**************************************    Editable data for schet excel end    **************************************/
              /*********************************************************************************************************************/


              if ($action == 'save' && $doctype == 'excel') $fileHelper->saveExcelFile($objPHPExcel, 'attachment');

              else if (($action == 'save' && $doctype == 'pdf') || $action == 'print'){
                if ($action == 'save') $fileHelper->savePDFFile($objPHPExcel, 'attachment');
                else if ($action == 'print') $fileHelper->savePDFFile($objPHPExcel, 'inline');
              }
                


           if ($action == 'email'){
              if ($email == null) $email = Yii::$app->user->identity->email;
              $fileHelper->sendEmail($objPHPExcel, $email);
              return $this->redirect(['index']);
          }
        } else throw new NotFoundHttpException('Этот документ вам не принадлежит.');
      } else throw new NotFoundHttpException('Произошла ошибка свяжитесь с администратором.');
    }

    /**
     * Finds the Act model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Act the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Act::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
