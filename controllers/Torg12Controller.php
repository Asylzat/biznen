<?php

namespace app\controllers;

use Yii;
use app\models\Torg12;
use app\models\Torg12Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\models\Schet;

/**
 * Torg12Controller implements the CRUD actions for Torg12 model.
 */
class Torg12Controller extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view', 'delete', 'create', 'file'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['update', 'create', 'file', 'delete'],
                        'allow' => true,
                        'roles' => ['author'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Torg12 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Torg12Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Torg12 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Torg12 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($docId = null)
    {
        $model = new Torg12();

        if ($docId != null && ($document = \app\models\Document::findOne($docId)) !== null) {

            if ($document->createdBy != Yii::$app->user->getId()) 
                throw new NotFoundHttpException('Документ создан не вами.' . $document->createdBy);
        } else {
            $docId = null;
            $document = new \app\models\Document();
        }
        if ($model->load(Yii::$app->request->post())) {

            $model->createdBy = Yii::$app->user->getId();
            if ($docId != null) {

                $model->id_document = $document->id;
                if ($model->save(false)){
                    \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'torg12_id');
                    return $this->redirect('/document');
                } else throw new NotFoundHttpException('Не удалось сохранить торг-12.');
            } else {
                $document->load(Yii::$app->request->post());
                $document->createdBy = Yii::$app->user->getId();
                if ($document->save(false)){
                    $model->id_document = $document->id;
                    if ($model->save(false)){
                        \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'torg12_id');
                        return $this->redirect(['/document']);
                    } else throw new NotFoundHttpException('Не удалось сохранить торг-12.');
                } else throw new NotFoundHttpException('Не смог создать документ, обратитесь к администратору.');
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'document' => $document,
                'docId' => $docId,
            ]);
        }
    }
    
    /**
     * Updates an existing Torg12 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $document = $model->document;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $document->load(Yii::$app->request->post());
            $document->save(false);

            if ($model->idnum == '') $model->idnum = $model->id;

            $model->save(false);

            \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'torg12_id');

            return $this->render('update', [
                'model' => $model,
                'document' => $document,
                'docId' => null,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'document' => $document,
                'docId' => null,
            ]);
        }
    }

    public function actionFile($id, $action, $email = null, $doctype = null)
    {

        $model = $this->findModel($id);
        if ($model != null) {

          if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['author'])) 
            if ($model->createdBy == Yii::$app->user->getId()) {
              error_reporting(E_ALL);
              ini_set('display_errors', TRUE);
              ini_set('display_startup_errors', TRUE);


              $fileHelper = new Schet();
              $fileHelper->pathPHPExcel = str_replace('/public_html', '', $_SERVER['DOCUMENT_ROOT']) . '/doc_create';
              require_once $fileHelper->pathPHPExcel . '/Classes/PHPExcel.php';


              $rowAdded = 0;

              /*********************************************************************************************************************/
              /**************************************    Editable data for torg12 excel    *****************************************/
              /*********************************************************************************************************************/

              $objPHPExcel= \PHPExcel_IOFactory::load($fileHelper->pathPHPExcel . '/template/torg12v.xls');

              $objPHPExcel->setActiveSheetIndex(0);
              $objWorksheet = $objPHPExcel->getActiveSheet();
              $objWorksheet->getStyle('A1:AN100')->applyFromArray($fileHelper->style()['clear']);


             $prodavesBank = '';
             $pokupatelBank = '';
            foreach($model->document->prodaves->banks as $current){
              $prodavesBank .=  ', р/с ' . $current->ras_schet . ' в банке ' . $current->name . ', БИК ' . $current->bik . ', к/с ' . $current->kor_schet;           
            }

            foreach($model->document->pokupatel->banks as $current){
              $pokupatelBank .=  ', р/с ' . $current->ras_schet . ' в банке ' . $current->name . ', БИК ' . $current->bik . ', к/с ' . $current->kor_schet;           
            }

            $objWorksheet->setCellValue('AL' . (4 + $rowAdded), $model->document->prodaves->okpo)
                          ->setCellValue('B' . (3 + $rowAdded), $model->document->prodaves->name_contr . ', ИНН ' .  $model->document->prodaves->inn . 
                            ', ' .  $model->document->prodaves->kpp . ', ' .  $model->document->prodaves->ur_adress . ', ' .  
                            $model->document->prodaves->fak_adress . ' тел.: ' . $model->document->prodaves->phone . $prodavesBank)
                          ->setCellValue('D' . (8 + $rowAdded), $model->delivsend)
                          ->setCellValue('AL' . (8 + $rowAdded), $model->sendokpo)
                          ->setCellValue('D' . (10 + $rowAdded), $model->delivget)
                          ->setCellValue('AL' . (10 + $rowAdded), $model->getokpo)
                          ->setCellValue('D' . (12 + $rowAdded), $model->document->pokupatel->name_contr . ', ИНН ' .  $model->document->pokupatel->inn . 
                            ', ' .  $model->document->pokupatel->kpp . ', ' .  $model->document->pokupatel->ur_adress . ', ' .  
                            $model->document->pokupatel->fak_adress . ' тел.: ' . $model->document->pokupatel->phone . $pokupatelBank)
                          ->setCellValue('K' . (17 + $rowAdded), $model->idnum)
                          ->setCellValue('O' . (17 + $rowAdded), $model->date);


              /*********************************************************************************************************************/
              /**************************************    torg12 service    *********************************************************/

            $counter = 0;
            foreach($model->document->schet->services as $current){
              
              if ($counter > 0){
                $rowAdded++;
                $objWorksheet->insertNewRowBefore(23 + $rowAdded, 1);
              }

              $objWorksheet->mergeCells('C'.(23 + $rowAdded).':F'.(23 + $rowAdded));
              $objWorksheet->mergeCells('H'.(23 + $rowAdded).':K'.(23 + $rowAdded));
              $objWorksheet->mergeCells('N'.(23 + $rowAdded).':P'.(23 + $rowAdded));
              $objWorksheet->mergeCells('R'.(23 + $rowAdded).':U'.(23 + $rowAdded));
              $objWorksheet->mergeCells('V'.(23 + $rowAdded).':W'.(23 + $rowAdded));
              $objWorksheet->mergeCells('X'.(23 + $rowAdded).':Y'.(23 + $rowAdded));
              $objWorksheet->mergeCells('Z'.(23 + $rowAdded).':AD'.(23 + $rowAdded));
              $objWorksheet->mergeCells('AE'.(23 + $rowAdded).':AG'.(23 + $rowAdded));
              $objWorksheet->mergeCells('AH'.(23 + $rowAdded).':AJ'.(23 + $rowAdded));
              $objWorksheet->mergeCells('AK'.(23 + $rowAdded).':AL'.(23 + $rowAdded));

              $objWorksheet->setCellValue('B'.(23 + $rowAdded), $current->number)
              ->setCellValue('C' . (23 + $rowAdded), $current->title); 
              /*$objPHPExcel->getActiveSheet()->setCellValue('V' . (23 + $rowAdded), $current->count); 
              $objPHPExcel->getActiveSheet()->setCellValue('Y' . (23 + $rowAdded), $current->code_name);              
              $objPHPExcel->getActiveSheet()->setCellValue('AB' . (23 + $rowAdded), $current->prize);              
              $objPHPExcel->getActiveSheet()->setCellValue('AG' . (23 + $rowAdded), $current->sum);*/              

              //$objPHPExcel->getActiveSheet()->getStyle('B'.(23 + $rowAdded).':AN'.(23 + $rowAdded))->applyFromArray($fileHelper->style()['fontNormal']);
              $objWorksheet->getStyle('V'.(23 + $rowAdded).':AD'.(23 + $rowAdded))->applyFromArray($fileHelper->style()['fontLeft']);
              $objWorksheet->getStyle('AH'.(23 + $rowAdded).':AL'.(23 + $rowAdded))->applyFromArray($fileHelper->style()['fontLeft']);
              //$objPHPExcel->getActiveSheet()->getStyle('AB'.(23 + $rowAdded).':AF'.(23 + $rowAdded))->applyFromArray($fileHelper->style()['fontRight']);
              //$objPHPExcel->getActiveSheet()->getStyle('AG'.(23 + $rowAdded).':AL'.(23 + $rowAdded))->applyFromArray($fileHelper->style()['fontRight']);

              $counter++;

            }

              /*$objPHPExcel->getActiveSheet()->setCellValue('AG' . (24 + $rowAdded), $model->document->schet->sum);              
              $objPHPExcel->getActiveSheet()->setCellValue('AG' . (26 + $rowAdded), $model->document->schet->sum);              
              $objPHPExcel->getActiveSheet()->setCellValue('B' . (27 + $rowAdded), 'Всего наименований '.$counter.', на сумму '.$model->document->schet->sum.' руб.');              

              $objPHPExcel->getActiveSheet()->setCellValue('H'.(36 + $rowAdded), $model->document->prodaves->gen_director);
              $objPHPExcel->getActiveSheet()->setCellValue('AF'.(36 + $rowAdded), $model->document->prodaves->glav_buhgalter);*/



              /*********************************************************************************************************************/
              /**************************************    Editable data for torg12 excel end    *************************************/
              /*********************************************************************************************************************/

              if ($action == 'save' && $doctype == 'excel') $fileHelper->saveExcelFile($objPHPExcel, 'attachment');

              else if (($action == 'save' && $doctype == 'pdf') || $action == 'print'){
                if ($action == 'save') $fileHelper->savePDFFile($objPHPExcel, 'attachment', true);
                else if ($action == 'print') $fileHelper->savePDFFile($objPHPExcel, 'inline', true);
              }
                


           if ($action == 'email'){
              if ($email == null) $email = Yii::$app->user->identity->email;
              $fileHelper->sendEmail($objPHPExcel, $email);
              return $this->redirect(['index']);
          }
        } else throw new NotFoundHttpException('Этот документ вам не принадлежит.');
      } else throw new NotFoundHttpException('Произошла ошибка свяжитесь с администратором.');
    }

    /**
     * Deletes an existing Torg12 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->document !== null) if ($model->document->schet === null) $model->document->delete();
        if ($model->services !== null) {
          foreach ($model->services as $current) {
            $current->delete();
          }
        }
        $model->delete();

        return $this->redirect(['document']);
    }

    /**
     * Finds the Torg12 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Torg12 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Torg12::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
