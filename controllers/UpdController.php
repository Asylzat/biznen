<?php

namespace app\controllers;

use Yii;
use app\models\Upd;
use app\models\UpdSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\models\Schet;

/**
 * UpdController implements the CRUD actions for Upd model.
 */
class UpdController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view', 'delete', 'create', 'file'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['index', 'update', 'view', 'delete', 'create', 'file'],
                        'allow' => true,
                        'roles' => ['author'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Upd models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UpdSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Upd model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Upd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($docId = null)
    {
        $model = new Upd();

        if ($docId != null && ($document = \app\models\Document::findOne($docId)) !== null) {

            if ($document->createdBy != Yii::$app->user->getId()) 
                throw new NotFoundHttpException('Документ создан не вами.' . $document->createdBy);
        } else {
            $docId = null;
            $document = new \app\models\Document();
        }
        if ($model->load(Yii::$app->request->post())) {

            $model->createdBy = Yii::$app->user->getId();
            if ($docId != null) {

                $model->id_document = $document->id;
                if ($model->save(false)){
                    \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'upd_id');
                    return $this->redirect('/document');
                } else throw new NotFoundHttpException('Не удалось сохранить УПД.');
            } else {
                $document->load(Yii::$app->request->post());
                $document->createdBy = Yii::$app->user->getId();
                if ($document->save(false)){
                    $model->id_document = $document->id;
                    if ($model->save(false)){
                        \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'upd_id');
                        return $this->redirect(['/document']);
                    } else throw new NotFoundHttpException('Не удалось сохранить УПД.');
                } else throw new NotFoundHttpException('Не смог создать документ, обратитесь к администратору.');
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'document' => $document,
                'docId' => $docId,
            ]);
        }
    }

    /**
     * Updates an existing Upd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $document = $model->document;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $document->load(Yii::$app->request->post());
            $document->save(false);

            if ($model->idnum == '') $model->idnum = $model->id;

            $model->save(false);

            \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'upd_id');

            return $this->render('update', [
                'model' => $model,
                'document' => $document,
                'docId' => null,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'document' => $document,
                'docId' => null,
            ]);
        }
    }

    /**
     * Deletes an existing Upd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->document !== null) if ($model->document->schet === null) $model->document->delete();
        if ($model->services !== null) {
          foreach ($model->services as $current) {
            $current->delete();
          }
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    public function actionFile($id, $action, $email = null, $doctype = null)
    {

        $model = $this->findModel($id);
        if ($model != null) {

          if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['author'])) 
            if ($model->createdBy == Yii::$app->user->getId()) {
              error_reporting(E_ALL);
              ini_set('display_errors', TRUE);
              ini_set('display_startup_errors', TRUE);


              $fileHelper = new Schet();
              $fileHelper->pathPHPExcel = str_replace('/public_html', '', $_SERVER['DOCUMENT_ROOT']) . '/doc_create';
              require_once $fileHelper->pathPHPExcel . '/Classes/PHPExcel.php';


              $rowAdded = 0;

              /*********************************************************************************************************************/
              /**************************************    Editable data for torg12 excel    *****************************************/
              /*********************************************************************************************************************/

              $objPHPExcel= \PHPExcel_IOFactory::load($fileHelper->pathPHPExcel . '/template/upd.xls');

              $objPHPExcel->setActiveSheetIndex(0);
              $objWorksheet = $objPHPExcel->getActiveSheet();
              $objWorksheet->getStyle('A1:BF100')->applyFromArray($fileHelper->style()['clear']);




            
             



              /*********************************************************************************************************************/
              /**************************************    Editable data for torg12 excel end    *************************************/
              /*********************************************************************************************************************/

              if ($action == 'save' && $doctype == 'excel') $fileHelper->saveExcelFile($objPHPExcel, 'attachment');

              else if (($action == 'save' && $doctype == 'pdf') || $action == 'print'){
                if ($action == 'save') $fileHelper->savePDFFile($objPHPExcel, 'attachment', true);
                else if ($action == 'print') $fileHelper->savePDFFile($objPHPExcel, 'inline', true);
              }
                


           if ($action == 'email'){
              if ($email == null) $email = Yii::$app->user->identity->email;
              $fileHelper->sendEmail($objPHPExcel, $email);
              return $this->redirect(['index']);
          }
        } else throw new NotFoundHttpException('Этот документ вам не принадлежит.');
      } else throw new NotFoundHttpException('Произошла ошибка свяжитесь с администратором.');
    }


    /**
     * Finds the Upd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Upd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Upd::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
