<?php

namespace app\controllers;

use Yii;
use app\models\PlatejPoruch;
use app\models\PlatejPoruchSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PlatejPoruchController implements the CRUD actions for PlatejPoruch model.
 */
class PlatejPoruchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view', 'delete', 'create'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['index', 'update', 'view', 'delete', 'create'],
                        'allow' => true,
                        'roles' => ['author'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PlatejPoruch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlatejPoruchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PlatejPoruch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PlatejPoruch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($docId = null)
    {
        $model = new PlatejPoruch();

        if ($docId != null && ($document = \app\models\Document::findOne($docId)) !== null) {

            if ($document->createdBy != Yii::$app->user->getId()) 
                throw new NotFoundHttpException('Документ создан не вами.' . $document->createdBy);
        } else {
            $docId = null;
            $document = new \app\models\Document();
        }
        if ($model->load(Yii::$app->request->post())) {

            $model->createdBy = Yii::$app->user->getId();
            if ($docId != null) {

                $model->id_document = $document->id;
                if ($model->save(false)){
                    \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'platej_poruch_id');
                    return $this->redirect('/document');
                } else throw new NotFoundHttpException('Не удалось сохранить торг-12.');
            } else {
                $document->load(Yii::$app->request->post());
                $document->createdBy = Yii::$app->user->getId();
                if ($document->save(false)){
                    $model->id_document = $document->id;
                    if ($model->save(false)){
                        \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'platej_poruch_id');
                        return $this->redirect(['/document']);
                    } else throw new NotFoundHttpException('Не удалось сохранить торг-12.');
                } else throw new NotFoundHttpException('Не смог создать документ, обратитесь к администратору.');
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'document' => $document,
                'docId' => $docId,
            ]);
        }
    }

    /**
     * Updates an existing PlatejPoruch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $document = $model->document;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $document->load(Yii::$app->request->post());
            $document->save(false);

            if ($model->idnum == '') $model->idnum = $model->id;

            $model->save(false);

            \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'platej_poruch_id');

            return $this->render('update', [
                'model' => $model,
                'document' => $document,
                'docId' => null,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'document' => $document,
                'docId' => null,
            ]);
        }
    }

    /**
     * Deletes an existing PlatejPoruch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->document !== null) if ($model->document->schet === null) $model->document->delete();
        if ($model->services !== null) {
          foreach ($model->services as $current) {
            $current->delete();
          }
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PlatejPoruch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PlatejPoruch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PlatejPoruch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
