<?php

namespace app\controllers;

use Yii;
use app\models\Schet;
use app\models\SchetSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * SchetController implements the CRUD actions for Schet model.
 */
class SchetController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view', 'delete', 'create', 'file'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['index', 'update', 'create', 'file', 'delete'],
                        'allow' => true,
                        'roles' => ['author'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Schet models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SchetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionFile($id, $action, $email = null, $doctype = null)
    {

        $model = $this->findModel($id);
        if ($model != null) {

          if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['author'])) 
            if ($model->createdBy == Yii::$app->user->getId()) {
              error_reporting(E_ALL);
              ini_set('display_errors', TRUE);
              ini_set('display_startup_errors', TRUE);


              $fileHelper = new Schet();
              $fileHelper->pathPHPExcel = str_replace('/public_html', '', $_SERVER['DOCUMENT_ROOT']) . '/doc_create';
              require_once $fileHelper->pathPHPExcel . '/Classes/PHPExcel.php';

              $rowAdded = 0;

              /*********************************************************************************************************************/
              /**************************************    Editable data for schet excel    ******************************************/
              /*********************************************************************************************************************/


              $objPHPExcel= \PHPExcel_IOFactory::load($fileHelper->pathPHPExcel . '/template/schet.xls');

             $objPHPExcel->setActiveSheetIndex(0);
              $objWorksheet = $objPHPExcel->getActiveSheet();
              $objWorksheet->getStyle('A1:AN100')->applyFromArray($fileHelper->style()['clear']);



            $counter = 0;
            foreach($model->document->prodaves->banks as $current){
              if ($counter > 0){
                $objWorksheet->insertNewRowBefore(5 + $rowAdded, 3);
                $rowAdded += 3;
              }

              $objWorksheet->setCellValue('B' . (2 + $rowAdded), $current->name)
              ->setCellValue('Z' . (2 + $rowAdded), $current->bik)
              ->setCellValue('Z' . (3 + $rowAdded), $current->kor_schet)
              ->setCellValue('B'.(4 + $rowAdded), 'Банк получателя')
              ->setCellValue('U'.(2 + $rowAdded), 'БИК')
              ->setCellValue('U'.(3 + $rowAdded), 'Сч. №')

              ->mergeCells('B'.(2 + $rowAdded).':T'.(3 + $rowAdded))
              ->mergeCells('B'.(4 + $rowAdded).':T'.(4 + $rowAdded))
              ->mergeCells('U'.(2 + $rowAdded).':Y'.(2 + $rowAdded))
              ->mergeCells('U'.(3 + $rowAdded).':Y'.(4 + $rowAdded))
              ->mergeCells('Z'.(2 + $rowAdded).':AM'.(2 + $rowAdded))
              ->mergeCells('Z'.(3 + $rowAdded).':AM'.(4 + $rowAdded));

              $objWorksheet->getStyle('B'.(4 + $rowAdded).':T'.(4 + $rowAdded))->applyFromArray($fileHelper->style()['borderTopNone']);
              $objWorksheet->getStyle('B'.(4 + $rowAdded).':T'.(4 + $rowAdded))->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
              $objWorksheet->getStyle('Z'.(3 + $rowAdded).':AM'.(4 + $rowAdded))->applyFromArray($fileHelper->style()['borderTopNone']);


              $counter++;            
            }

            /*$objPHPExcel->getActiveSheet()->getStyle('A1');
            $objPHPExcel->getActiveSheet()->duplicateStyle(
              $objPHPExcel->getActiveSheet()->g‌etStyle('A1'),
              'A2:A10'); */

            $objWorksheet->setCellValue('E' . (5 + $rowAdded), $model->document->prodaves->inn)
                          ->setCellValue('N' . (5 + $rowAdded), $model->document->prodaves->kpp)
                          ->setCellValue('B' . (6 + $rowAdded), $model->document->prodaves->name_contr)
                          ->setCellValue('B' . (10 + $rowAdded), 'Счет на оплату № ' . $model->id . ' от ' . $model->date . ' г.')
                          ->setCellValue('G' . (14 + $rowAdded), $model->document->prodaves->name_contr . ', ИНН ' .  $model->document->prodaves->inn . 
                            ', КПП ' .  $model->document->prodaves->kpp . ', ' .  $model->document->prodaves->ur_adress . ', ' .  
                            $model->document->prodaves->fak_adress)
                          ->setCellValue('G' . (17 + $rowAdded), $model->document->pokupatel->name_contr . ', ИНН ' .  $model->document->pokupatel->inn . 
                            ', КПП ' .  $model->document->pokupatel->kpp . ', ' .  $model->document->pokupatel->ur_adress . ', ' .  
                            $model->document->pokupatel->fak_adress);

              /*********************************************************************************************************************/
              /**************************************    schet service    **********************************************************/

            $counter = 0;
            foreach($model->document->schet->services as $current){
              
              if ($counter > 0){
                $rowAdded++;
                $objWorksheet->insertNewRowBefore(23 + $rowAdded, 1);
              }

              $objWorksheet->mergeCells('B'.(23 + $rowAdded).':C'.(23 + $rowAdded));
              $objWorksheet->mergeCells('D'.(23 + $rowAdded).':U'.(23 + $rowAdded));
              $objWorksheet->mergeCells('V'.(23 + $rowAdded).':X'.(23 + $rowAdded));
              $objWorksheet->mergeCells('Y'.(23 + $rowAdded).':AA'.(23 + $rowAdded));
              $objWorksheet->mergeCells('AB'.(23 + $rowAdded).':AF'.(23 + $rowAdded));
              $objWorksheet->mergeCells('AG'.(23 + $rowAdded).':AL'.(23 + $rowAdded));

              $objWorksheet->setCellValue('B'.(23 + $rowAdded), $current->number)
              ->setCellValue('D' . (23 + $rowAdded), $current->title)
              ->setCellValue('V' . (23 + $rowAdded), $current->count) 
              ->setCellValue('Y' . (23 + $rowAdded), $current->code_name)              
              ->setCellValue('AB' . (23 + $rowAdded), $current->prize)            
              ->setCellValue('AG' . (23 + $rowAdded), $current->sum);              

              $objWorksheet->getStyle('B'.(23 + $rowAdded).':AN'.(23 + $rowAdded))->applyFromArray($fileHelper->style()['fontNormal']);
              $objWorksheet->getStyle('D'.(23 + $rowAdded).':U'.(23 + $rowAdded))->applyFromArray($fileHelper->style()['fontLeft']);
              $objWorksheet->getStyle('AB'.(23 + $rowAdded).':AF'.(23 + $rowAdded))->applyFromArray($fileHelper->style()['fontRight']);
              $objWorksheet->getStyle('AG'.(23 + $rowAdded).':AL'.(23 + $rowAdded))->applyFromArray($fileHelper->style()['fontRight']);

              $counter++;

            }

              $objWorksheet->setCellValue('AG' . (24 + $rowAdded), $model->document->schet->sum)          
              ->setCellValue('AG' . (26 + $rowAdded), $model->document->schet->sum)  
              ->setCellValue('B' . (27 + $rowAdded), 'Всего наименований '.$counter.', на сумму '.$model->document->schet->sum.' руб.')           

              ->setCellValue('H'.(36 + $rowAdded), $model->document->prodaves->gen_director)
              ->setCellValue('AF'.(36 + $rowAdded), $model->document->prodaves->glav_buhgalter);


              /*$objDrawing = new \PHPExcel_Worksheet_Drawing();
                $objDrawing->setName('Logo ');
                $objDrawing->setDescription('Logo ');
                $objDrawing->setPath('upload/6img1.png');
                $objDrawing->setResizeProportional(true);
                $objDrawing->setWidth(240);
                $objDrawing->setCoordinates('B2');
                $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());*/


              //$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

              $objDrawing = new \PHPExcel_Worksheet_Drawing();
              $objDrawing->setName('Logo');
              $objDrawing->setDescription('Logo');
              $logo = 'upload/6img1.png'; // Provide path to your logo file
              $objDrawing->setPath($logo);

              $objDrawing->setOffsetX(8);    // setOffsetX works properly
              $objDrawing->setOffsetY(300);  //setOffsetY has no effect
              $objDrawing->setCoordinates('B4');
              $objDrawing->setHeight(75); // logo height
              $objDrawing->setWorksheet($objWorksheet); 



              /*********************************************************************************************************************/
              /**************************************    Editable data for schet excel end    **************************************/
              /*********************************************************************************************************************/


              if ($action == 'save' && $doctype == 'excel') {
                $fileHelper->saveExcelFile($objPHPExcel, 'attachment');

              } else if (($action == 'save' && $doctype == 'pdf') || $action == 'print'){
                if ($action == 'save') $fileHelper->savePDFFile($objPHPExcel, 'attachment');
                else if ($action == 'print') {

                  /*$html = '<h1><a name="top"></a>mPDF</h1>';
                  include($fileHelper->pathPHPExcel .'/mpdf60/mpdf.php');
                  $mpdf=new \mPDF(); 
                  $mpdf->WriteHTML($html);
                  $mpdf->Output();
                  exit;*/
                  $fileHelper->savePDFFile($objPHPExcel, 'inline');
                }
              }
                


           if ($action == 'email'){
              if ($email == null) $email = Yii::$app->user->identity->email;
              $fileHelper->sendEmail($objPHPExcel, $email);
              return $this->redirect(['index']);
          }
        } else throw new NotFoundHttpException('Этот документ вам не принадлежит.');
      } else throw new NotFoundHttpException('Произошла ошибка свяжитесь с администратором.');
    }


    /**
     * Displays a single Schet model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Schet model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Schet();
        $document = new \app\models\Document();
        $model->date = date("d.m.Y");


        if ($model->load(Yii::$app->request->post())) {

            $document->load(Yii::$app->request->post());
            $document->createdBy = Yii::$app->user->getId();
            $model->createdBy = Yii::$app->user->getId();
            if ($document->save(false)){
                $model->id_document = $document->id;
                if ($model->save()){
                  
                    \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'schet_id');

                    return $this->redirect(['index']);
                } else throw new NotFoundHttpException('Не удалось сохранить счет.');
            } else throw new NotFoundHttpException('Не смог создать документ, обратитесь к администратору.');
        } else {
            return $this->render('create', [
                'model' => $model,
                'document' => $document,
            ]);
        }
    }

    /**
     * Updates an existing Schet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $document = $model->document;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $document->load(Yii::$app->request->post());
            $document->save(false);

            if ($model->idnum == '') $model->idnum = $model->id;

            $model->save(false);

            \app\models\Service::serviceData(Yii::$app->request->post(), $model->id, 'schet_id');

            return $this->render('update', [
                'model' => $model,
                'document' => $document,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'document' => $document,
            ]);
        }
    }

    /**
     * Deletes an existing Schet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->document !== null) $model->document->delete();
        if ($model->services !== null) {
          foreach ($model->services as $current) {
            $current->delete();
          }
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Schet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Schet the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Schet::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
