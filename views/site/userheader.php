<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;


$this->registerJs("

    if ($.session.get('toggled') == 'true'){
        $('#wrapper').toggleClass('toggled-2');    
    }


 
$('#menu-toggle').click(function(e) {
        e.preventDefault();
        $('#wrapper').toggleClass('toggled');
    });
     $('#menu-toggle-2').click(function(e) {
        e.preventDefault();
        $('#wrapper').toggleClass('toggled-2');
        $('#menu ul').hide();
        if ($.session.get('toggled') == 'true') $.session.set('toggled', false); else $.session.set('toggled', 'true');
    });

     /*function initMenu() {
      $('#menu ul').hide();
      $('#menu ul').children('.current').parent().show();
      //$('#menu ul:first').show();
      $('#menu li a').click(
        function() {
          var checkElement = $(this).next();
          if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            return false;
            }
          if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('#menu ul:visible').slideUp('normal');
            checkElement.slideDown('normal');
            return false;
            }
          }
        );
      }
    $(document).ready(function() {initMenu();});*/

    ");
$this->registerJsFile('/js/jquery.session.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/script.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile('/css/userpage.css');

?>


</div>

<div class="showfoto"></div>


    <?php

        NavBar::begin([
            'brandLabel' => '
                    
                    <img src="/img/ic6.png" /><span><p>Biznen</p><p>Сервис выставления счетов</p></span>
                    </a>
                    <a>
                    <button type="button" class="left-btn navbar-toggle" data-toggle="collapse" id="menu-toggle">
                      <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                    </button>
                    ',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-default navbar-fixed-top no-margin user-nav',
            ],
            'innerContainerOptions' => ['class' => 'container-fluid'],
        ]);

    ?>

                    <ul class="nav navbar-nav nav-site hidden-xs">
                        <li class="active"><button class="left-btn navbar-toggle collapse in" data-toggle="collapse" id="menu-toggle-2"> 
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                        </button></li>
                    </ul>


    <?php

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-left'],
            'items' => [
                   '
                    <ul class="navbar-nav navbar-left nav">
                            
                            <li><a href="#"><span class="icon-border icon-transparent icomoon icomoon-newspaper"></span></a></li>
                            <li><a href="#"><span class="icon-border icon-transparent glyphicon glyphicon-envelope"></span></a></li>
                            
                        </ul>
                    '
            ],
        ]);

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                //['label' => 'Как это работает', 'url' => ['/site/howit']],
                Yii::$app->user->isGuest ? (
                    ['label' => 'Войти', 'url' => ['/site/login']]
                ) : (
                    '
                    <ul class="navbar-nav navbar-left nav hidden-xs">
                            
                        <li><a href="#"><span class="icon-border icon-transparent icomoon icomoon-book"></span></a></li>
                        <li><a href="#"><span class="icon-border icon-transparent glyphicon glyphicon-question-sign"></span></a></li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="icon-border glyphicon glyphicon-user"></span> ' . Yii::$app->user->identity->username . ' <span class="caret"></span>
                        </a>
                          <ul class="dropdown-menu">
                            <li><a href="/profile">Профиль ' . Yii::$app->user->identity->username . '</a></li>
                            <li><a href="/document">Документооборот</a></li>
                            <li role="separator" class="divider"></li>
                            <li style="background: rgb(27, 39, 50) none repeat scroll 0% 0%;">'
                                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                                . Html::submitButton(
                                    '<span class="glyphicon glyphicon-log-out"></span> Выход (' . Yii::$app->user->identity->username . ')',
                                    ['class' => 'btn btn-link']
                                ) 
                                . Html::endForm() . '</li>
                          </ul>
                        </li>
                        
                    </ul>
                    <li class="hidden-sm hidden-md hidden-lg"><a href="/profile">Профиль ' . Yii::$app->user->identity->username . '</a></li>
                    <li class="hidden-sm hidden-md hidden-lg"><a href="/document">Документооборот</a></li>
                    <li class="hidden-sm hidden-md hidden-lg" role="separator" class="divider"></li>
                    <li class="hidden-sm hidden-md hidden-lg" style="background: rgb(27, 39, 50) none repeat scroll 0% 0%;">'
                        . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                        . Html::submitButton(
                            '<span class="glyphicon glyphicon-log-out"></span> Выход (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link']
                        ) 
                        . Html::endForm() . '</li>
                    '
                ),
                Yii::$app->user->isGuest ? (
                            ['label' => 'Регистрация', 'url' => ['site/signup'], 'options' => ['class' => 'dl-register'] ]
                        ) : ( '' ),
            ],
        ]);
        NavBar::end();
    ?>




    <div class="toggled-2" id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu">

                <li <?php if ($_SERVER['REQUEST_URI'] == "/schet/create") echo 'class="active"'; ?> >
                    <a href="/schet/create"><span class="fa-stack fa-lg pull-left"><i class="icomoon icomoon-news fa-stack-1x "></i></span> Выставить счет</a>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == "/schet") echo 'class="active"'; ?> >
                    <a href="/schet"><span class="fa-stack fa-lg pull-left"><i class="icomoon icomoon-files-empty fa-stack-1x "></i></span> Счета</a>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == "/contragent") echo 'class="active"'; ?> >
                    <a href="/contragent"><span class="fa-stack fa-lg pull-left"><i class="icomoon icomoon-office fa-stack-1x"></i></span> Контрагенты</a>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == "/document") echo 'class="active"'; ?> >
                    <a href="/document"> <span class="fa-stack fa-lg pull-left"><i class="icomoon icomoon-book-bookmark fa-stack-1x "></i></span> Документооборот</a>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == "/act/create") echo 'class="active"'; ?> >
                    <a href="/act/create"><span class="fa-stack fa-lg pull-left"><i class="icomoon icomoon-document-text fa-stack-1x "></i></span> Акты</a>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == "/torg12/create") echo 'class="active"'; ?> >
                    <a href="/torg12/create"><span class="fa-stack fa-lg pull-left"><i class="icomoon icomoon-newspaper3 fa-stack-1x "></i></span> Товарные накладные</a>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == "/schet-faktura/create") echo 'class="active"'; ?> >
                    <a href="/schet-faktura/create"><span class="fa-stack fa-lg pull-left"><i class="icomoon icomoon-file-text2 fa-stack-1x "></i></span> Счета-фактуры</a>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == "/platej-poruch/create") echo 'class="active"'; ?> >
                    <a href="/platej-poruch/create"><span class="fa-stack fa-lg pull-left"><i class="icomoon icomoon-note-list fa-stack-1x "></i></span> Платежное поручение</a>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == "/upd/create") echo 'class="active"'; ?> >
                    <a href="/upd/create"><span class="fa-stack fa-lg pull-left"><i class="icomoon icomoon-notebook-list fa-stack-1x "></i></span> УПД</a>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == "/user/file") echo 'class="active"'; ?> >
                    <a href="/user/file"><span class="fa-stack fa-lg pull-left"><i class="glyphicon glyphicon-pencil fa-stack-1x "></i></span> Подпись и печать</a>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == "/export") echo 'class="active"'; ?> >
                    <a href="/export"><span class="fa-stack fa-lg pull-left"><i class="glyphicon glyphicon-new-window fa-stack-1x "></i></span> Экспорт данных</a>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == "/statistic") echo 'class="active"'; ?> >
                    <a href="/statistic"><span class="fa-stack fa-lg pull-left"><i class="icomoon icomoon-stats-dots fa-stack-1x "></i></span> Статистика</a>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == "/balance") echo 'class="active"'; ?> >
                    <a href="/balance"><span class="fa-stack fa-lg pull-left"><i class="icomoon icomoon-banknote fa-stack-1x "></i></span> Выбор тарифного плана</a>
                </li>
            <div class="proba">У Вас пробная версия сервиса, осталось 30 дней.</div>

            </ul>
        </div><!-- /#sidebar-wrapper -->
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid xyz">
 

        <?= Breadcrumbs::widget([
            'homeLink'=>[ 'label' => 'Главная', 'url' => '/', ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
































