<?php

/* @var $this yii\web\View */

$this->title = 'Biznen для бухгалтеров';

$this->registerJs("
        $('.carousel').carousel({
          interval: 5000
        })
    ");

?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/header.php'); ?>


    <section id="home-banner"></section>


<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="/img/ic8.png" alt="slide1">
      <div class="carousel-caption">
        text1
      </div>
    </div>
    <div class="item">
      <img src="/img/ic8.png" alt="slide2">
      <div class="carousel-caption">
        text2
      </div>
    </div>
    <div class="item">
      <img src="/img/ic8.png" alt="slide3">
      <div class="carousel-caption">
        text3
      </div>
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>



<div class="container">

    <section id="home-hiw" class="row">
        <div class="col-md-3 col-lg-3 col-sm-6">
            <div>
                <img src="/img/ic16.png">
                <h4>Мобильность</h4>
                <div>Вы можете работать с системой из любой точки мира, круглосуточно.</div>
                <img src="/img/ic12.png" width="100%;">
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-6">
            <div>
                <img src="/img/ic15.png">
                <h4>Гибкость</h4>
                <div>Настройте программу под свои нужды! Загрузите свой логотип, подпись и печать — система полностью подстроится под вас.</div>
                <img src="/img/ic11.png" width="100%;">
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-6">
            <div>
                <img src="/img/ic14.png">
                <h4>Простота</h4>
                <div>Никаких заумных слов и сложных кнопок — удобный интерфейс и эргономичный дизайн понятны всем.</div>
                <img src="/img/ic10.png" width="100%;">
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-6">
            <div>
                <img src="/img/ic13.png">
                <h4 style="padding-top: 30px;">Скорость</h4>
                <div>Наша система позволяет быстро вводить данные и делать документы. Мы уважаем ваше время.</div>
                <img src="/img/ic9.png" width="100%;">
            </div>
        </div>

    </section>

</div>


    <section id="home-why-use">
     <div class="container">
        <h2> Возможности сервиса</h2>
        <div class="home-why-use2">Всё самое сложное мы берем на себя.</div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-6">
                <h3><span class="icon-border icon-transparent glyphicon glyphicon-user"></span> Автоматизация бизнеса</h3>
                <div>Краткий текст услуги... Борис остановился посреди комнаты, оглянулся, смахнул рукой соринки с рукава мундира и подошел к зеркалу, рассматривая свое красивое лицо.</div>
            </div>

            <div class="col-md-4 col-lg-4 col-sm-6">
                <h3><span class="icon-border icon-transparent glyphicon glyphicon-user"></span> Не требует особых знаний</h3>
                <div>Краткий текст услуги... Борис остановился посреди комнаты, оглянулся, смахнул рукой соринки с рукава мундира и подошел к зеркалу, рассматривая свое красивое лицо.</div>
            </div>

            <div class="col-md-4 col-lg-4 col-sm-6">
                <h3><span class="icon-border icon-transparent glyphicon glyphicon-user"></span> Экономия времени</h3>
                <div>Краткий текст услуги... Борис остановился посреди комнаты, оглянулся, смахнул рукой соринки с рукава мундира и подошел к зеркалу, рассматривая свое красивое лицо.</div>
            </div>

            <div class="col-md-4 col-lg-4 col-sm-6">
                <h3><span class="icon-border icon-transparent glyphicon glyphicon-user"></span> Доступность данных</h3>
                <div>Краткий текст услуги... Борис остановился посреди комнаты, оглянулся, смахнул рукой соринки с рукава мундира и подошел к зеркалу, рассматривая свое красивое лицо.</div>
            </div>

            <div class="col-md-4 col-lg-4 col-sm-6">
                <h3><span class="icon-border icon-transparent glyphicon glyphicon-user"></span> Удобное хранение документов</h3>
                <div>Краткий текст услуги... Борис остановился посреди комнаты, оглянулся, смахнул рукой соринки с рукава мундира и подошел к зеркалу, рассматривая свое красивое лицо.</div>
            </div>

            <div class="col-md-4 col-lg-4 col-sm-6">
                <h3><span class="icon-border icon-transparent glyphicon glyphicon-user"></span> Статистические данные</h3>
                <div>Краткий текст услуги... Борис остановился посреди комнаты, оглянулся, смахнул рукой соринки с рукава мундира и подошел к зеркалу, рассматривая свое красивое лицо.</div>
            </div>

        </div>
     </div>
    </section>

    <section id="home-where">
        <div class="container">
            <h2>Наши клиенты</h2>
            <div>Что они думают?</div>
            <img src="/img/ic22.png" />
            <h3>Лев Толстой</h3>
            <div class="home-where">Соня была тоненькая, миниатюрненькая брюнетка с мягким, отененным длинными ресницами взглядом, густою черною косою, 
                два раза обвивавшею ее голову, и желтоватым оттенком кожи на лице и в особенности на обнаженных худощавых, но грациозных мускулистых руках и шее.</div>
        </div>

    </section>

    <section id="home-testimonials">
     <div class="container">
        <h2>У вас еще остались вопросы?</h2>
        <div>оставьте ваши данные и наш менеджер свяжиться с вами.</div>
        <a class="btn my-btn">СВЯЗАТЬСЯ С НАМИ</a>
     </div>
    </section>



<?php echo \Yii::$app->view->renderFile('@app/views/site/footer.php'); ?>
