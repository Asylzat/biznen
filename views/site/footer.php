        

        <div class="container"><div class="scrollTop"><span class="glyphicon glyphicon-menu-up"></span></div></div>
</div>



<footer class="footer">


    <div class="container">
        <div class="row"> 

            <div class="col-xs-12 col-md-12"> 
            	<div class="row">
            		<div class="col-md-6 col-lg-6 col-sm-12">
            			<a class="navbar-brand" href="/" style="padding: 0px;">
            				<img src="/img/ic6.png"><span>
            				<p style="color: white;">Biznen</p>
            				<p>Сервис выставления счетов</p></span>
            			</a>
            		</div>
            		<div class="col-md-6 col-lg-6 col-sm-12 footer-social">
						    <span style="font-size: 23px;margin: -3px 0px;">

    <a href="#"><span class="icomoon icomoon-vk-with-circle"></span></a>
    <a href="#"><span class="icomoon icomoon-facebook-with-circle"></span></a>
    <a href="#"><span class="icomoon icomoon-linkedin-with-circle"></span></a>
</span>
						    <br>
				            <a><span class="glyphicon glyphicon glyphicon-earphone"></span> +7(499)343-06-30</a>
				            <a><span class="glyphicon glyphicon glyphicon-envelope"></span> info@biznen.com</a>
            		</div>
            	</div>
            </div> 
            <br>
            <hr class="col-xs-12 col-md-12" style="color:#8B959B;">
            <div class="col-xs-12 col-md-12 footer-copyright"> 
                <p><cite>© <?= date('Y') ?> Biznen. Все права защищены.</cite></p> 
            </div> 

        </div>
    </div>
</footer>

<div class="container">