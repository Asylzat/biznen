<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';
$this->registerJsFile("/assets/dee07ee5/js/bootstrap.js", ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile("/js/jquery.kladr.min.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile("/js/all.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile("http://api-maps.yandex.ru/2.1/?lang=ru_RU", ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile("/css/jquery.kladr.min.css");

if ($registered) $this->registerJs("$('#myModal').modal('show')");




        //<link href="css/all.css" rel="stylesheet">


?>
</div>

<div class="showfoto"></div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Вы успешно прошли регистрацию</h4>
      </div>
      <div class="modal-body">
        Мы отправили пароль вам на почту!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>



<section id="login-signup">
    <div class="login-logo">
        <a href="/"><img src="/img/ic6.png" /><span><p>Biznen</p><p>Сервис выставления счетов</p></span></a>
    </div>

    <div class="container">

        <div class="site-login">

            <div class="row"> 
                
                <div class="inner col-md-5"> 
                    <h2>Авторизация</h2> 
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'options' => ['class' => 'row'],
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                            'labelOptions' => ['class' => 'col-lg-12 control-label'],
                        ],
                    ]); ?>

                        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Логин(ИНН)'])->label(false) ?>

                        <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false) ?>

                        <?= $form->field($model, 'rememberMe', ['options' => ['class' => 'remMe']])->checkbox([
                            'template' => "<div class=\"col-lg-12\">{input} {label}</div>\n<div class=\"col-lg-12\">{error}</div>",
                        ]) ?>

                        <a href="/site/contact" class="help-block text-center" style="width: 50%;float: right;margin-bottom: 28px;">Забыли пароль?</a>

                        <div class="form-group">
                            <div class="col-lg-12">
                                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            </div>
                        </div>

                    <?php ActiveForm::end(); ?>
                    
                </div>

                <div class="inner col-md-5 col-md-offset-2">
                    <h2>Регистрация</h2>

                    <?php $form = ActiveForm::begin([
                        'id' => 'signup-form',
                        //'options' => ['class' => 'form-horizontal'],
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                            'labelOptions' => ['class' => 'col-lg-12 control-label'],
                        ],
                    ]); ?>

                        <?= $form->field($model2, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Логин(ИНН)'])->label(false) ?>

                        <?= $form->field($model2, 'name_contr')->textInput(['autofocus' => true, 'placeholder' => 'Наименование контрагента'])->label(false) ?>
                        <?= $form->field($model2, 'kpp')->textInput(['autofocus' => true, 'placeholder' => 'КПП'])->label(false) ?>
                        <?= $form->field($model2, 'ogrn')->textInput(['autofocus' => true, 'placeholder' => 'ОГРН'])->label(false) ?>

                        <?= $form->field($model2, 'ur_adress')->textInput(['autofocus' => true, 'placeholder' => 'Юридический адрес'])->label(false) ?>
                        <?= $form->field($model2, 'fak_adress')->textInput(['autofocus' => true, 'placeholder' => 'Фактический адрес'])->label(false) ?>

                        <?= $form->field($model2, 'gen_director')->textInput(['autofocus' => true, 'placeholder' => 'Генеральный директор (Ген. директор)'])->label(false) ?>

                        <?= $form->field($model2, 'phone')->textInput(['autofocus' => true, 'placeholder' => 'Телефон (тел.)'])->label(false) ?>
                        <?= $form->field($model2, 'email')->textInput(['autofocus' => true, 'placeholder' => 'Email'])->label(false) ?>


                        <div class="form-group">
                            <div class="col-lg-12">
                                <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                            </div>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>


            </div>

            <div class="login-footer"> BIZNEN <?= date('Y') ?> </div>


        </div>
    </div>
</section>



<div class="container">

