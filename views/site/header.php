<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

$this->registerCssFile('/css/helper.css');

?>

</div>
<div class="showfoto"></div>

<div class="wrap">

    

<?php

    $this->registerJs("
        $('.scrollTop').click(function(){
            $('body,html').animate({ scrollTop : 0 }, 500);
        });

        var bodyHeight = document.body.scrollHeight, pageScroll = 0;
        window.onscroll = function(){ 
            pageScroll = $(window).scrollTop();
            var shNavbar = $('.sh-navbar').offset();
            shNavbar = shNavbar.top + $('.sh-navbar').height();
            if ($(window).scrollTop() > 100) $('.scrollTop').show(); else $('.scrollTop').hide();
            if ($(window).scrollTop() > shNavbar && bodyHeight > window.innerHeight + shNavbar +100) { 
                $('.sh-navbar-fixed').addClass('navbar-fixed-top');
                $('.sh-navbar').css({ marginBottom : $('.sh-navbar-fixed').height() });
            } else { 
                $('.sh-navbar-fixed').removeClass('navbar-fixed-top');
                $('.sh-navbar').css({ marginBottom : 0 });
            }
        }"
        );

    NavBar::begin([
        'options' => [
            'class' => 'navbar-inverse sh-navbar',
        ],
    ]);

    ?>
<ul class="navbar-nav navbar-left nav">

            <li><a><span class="glyphicon glyphicon glyphicon-earphone"></span> +7(499)343-06-30</a></li>
            <li><a><span class="glyphicon glyphicon glyphicon-envelope"></span> info@biznen.com</a></li>
</ul>

<ul class="navbar-nav navbar-right nav" style="font-size: 23px;margin: -3px 0px;">

    <li><a href="#"><span class="icomoon icomoon-vk-with-circle"></span></a></li>
    <li><a href="#"><span class="icomoon icomoon-facebook-with-circle"></span></a></li>
    <li><a href="#"><span class="icomoon icomoon-linkedin-with-circle"></span></a></li>

</ul>
    <?php
    NavBar::end();

    NavBar::begin([
        'brandLabel' => '<img src="/img/ic6.png" /><span><p>Biznen</p><p>Сервис выставления счетов</p></span>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse dl-nav sh-navbar-fixed',
        ],
    ]);


    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'О ПРОЕКТЕ', 'url' => ['/site/howit']],
            ['label' => 'ДЕМО РЕЖИМ', 'url' => ['/site/howit']],
            ['label' => 'ПОДДЕРЖКА', 'url' => ['/site/howit']],
            ['label' => 'КОНТАКТЫ', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'ВХОД/РЕГИСТРАЦИЯ', 'url' => ['/site/login']]
            ) : (
                '
                <ul class="navbar-nav navbar-left nav hidden-xs">
                        
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="glyphicon glyphicon-user"></span>
                        <span class="caret"></span>
                    </a>
                      <ul class="dropdown-menu">
                        <li><a href="/profile">Профиль ' . Yii::$app->user->identity->username . '</a></li>
                        <li><a href="/document">Документооборот</a></li>
                        <li role="separator" class="divider"></li>
                        <li style="background: rgb(27, 39, 50) none repeat scroll 0% 0%;">'
                            . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                            . Html::submitButton(
                                '<span class="glyphicon glyphicon-log-out"></span> Выход (' . Yii::$app->user->identity->username . ')',
                                ['class' => 'btn btn-link']
                            ) 
                            . Html::endForm() . '</li>
                      </ul>
                    </li>
                    
                </ul>

                <li class="hidden-sm hidden-md hidden-lg"><a href="/profile">Профиль ' . Yii::$app->user->identity->username . '</a></li>
                <li class="hidden-sm hidden-md hidden-lg"><a href="/document">Документооборот</a></li>
                <li class="hidden-sm hidden-md hidden-lg" role="separator" class="divider"></li>
                <li class="hidden-sm hidden-md hidden-lg" style="background: rgb(27, 39, 50) none repeat scroll 0% 0%;">'
                    . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                    . Html::submitButton(
                        '<span class="glyphicon glyphicon-log-out"></span> Выход (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link']
                    ) 
                    . Html::endForm() . '</li>
                '
            ),
            /*Yii::$app->user->isGuest ? (
                        ['label' => 'Регистрация', 'url' => ['site/signup'], 'options' => ['class' => 'dl-register'] ]
                    ) : ( '' ),*/
        ],
    ]);
    NavBar::end();
    ?>

        <?= Breadcrumbs::widget([
            'homeLink'=>[ 'label' => 'Главная', 'url' => '/', ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>