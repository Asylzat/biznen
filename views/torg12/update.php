<?php

use yii\helpers\Html;

$this->title = 'ТОРГ-12 Редактировать - ' . $model->id;
?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>
<div class="torg12-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'document' => $document,
        'docId' => $docId,
    ]) ?>

</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
