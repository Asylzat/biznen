<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->registerJsFile('/js/bootstrap-datepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile('/css/bootstrap-datepicker.css');

$this->registerJs('
    $(document).ready(function(){
        $("#catout").on("pjax:end", function(){
            $.pjax.reload({ container : "#catin" });
        });

        $(".delete").click(function(event){
            if ($(this).attr("href") == "" || $(this).attr("href") == "#") return false;
            else $(this).attr("data-confirm", "Are you sure you want to delete this item?")
        });

    });

//0
$("#collapseField0").on("hidden.bs.collapse", function () {
  $(".collapseBt0").find("span").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
});

$("#collapseField0").on("shown.bs.collapse", function () {
  $(".collapseBt0").find("span").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
});

    ');
?>

<?php Pjax::begin(['id' => 'catout']); ?>

<div class="collapseBtF">
    <a class="collapseBt0" role="button" data-toggle="collapse" href="#collapseField0" aria-expanded="false" aria-controls="collapseField0">
      <span class="icon-border icon-blue glyphicon glyphicon-menu-up"></span> 
    </a> 
    <span class="icomoon icomoon-filter"></span> <span class="icText">Фильтр</span>
</div>
<div class="row myrow collapse in"  id="collapseField0">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['data-pjax'=>true, 'onchange' => "var container = $(this).closest('[data-pjax-container]');    $.pjax.submit(event, container)"],
    ]); ?>

        <div class="col-lg-2 col-md-2 col-sm-4">
            <?= $form->field($model, 'schetinn')->textInput(['autofocus' => true, 'placeholder' => 'По номеру, товара, оплата за...'])->label('Поиск счета') ?>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-4">
            <?= $form->field($model, 'prodaves')->textInput(['autofocus' => true, 'placeholder' => 'Поиск по наименованию, ИНН'])->label('Продавец') ?>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-4">
            <?= $form->field($model, 'pokupatel')->textInput(['autofocus' => true, 'placeholder' => 'Поиск по наименованию, ИНН'])->label('Покупатель') ?>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-4">
            <?= $form->field($model, 'dateFrom')->textInput(['autofocus' => true, 'placeholder' => 'Начальная дата'])->label('Период, с...') ?>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-4">
            <?= $form->field($model, 'dateTo')->textInput(['autofocus' => true, 'placeholder' => 'Конечная дата'])->label('по...') ?>
        </div>

    <?php ActiveForm::end(); ?>

    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4 margint20">
        <?= Html::submitButton('<span class="glyphicon glyphicon-search"></span> <span class="icTextBtn">Поиск</span> ', ['class' => 'btn linkBtn']) ?>
    </div>


</div>

<div class="row myrow up">
    <div class="col-lg-2 col-md-4 col-sm-4">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options' => ['data-pjax'=>true, 'onchange' => "var container = $(this).closest('[data-pjax-container]');    $.pjax.submit(event, container)"],
        ]); ?>


        <?= $form->field($model, 'pokupatel')->textInput(['autofocus' => true, 'placeholder' => 'поиск...'])->label(false) ?>

        <?php ActiveForm::end(); ?>

    </div>
    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
        <a class="btn linkBtn createDoc" data-toggle="modal" data-target="#documentModal" data-whatever="">
            <span class="glyphicon glyphicon-print"></span> <span class="icTextBtn">Новый</span>
        </a>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> <span class="icTextBtn">Редактировать</span>', ['#'], ['class' => 'btn linkBtn edit']) ?>
    </div>
    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
        <a class="btn linkBtn printBig" data-toggle="modal" data-target="#schetModal">
            <span class="glyphicon glyphicon-print"></span> <span class="icTextBtn">Печать</span>
        </a>
    </div>
    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
        <a class="btn linkBtn saveBig" data-toggle="modal" data-target="#schetModalEmail">
            <span class="glyphicon glyphicon-floppy-disk"></span> <span class="icTextBtn">E-mail</span>
        </a>
    </div>
    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
        <a href="#" class="btn linkBtn delete" title="Delete" aria-label="Delete" data-method="post" data-pjax="0">
            <span class="glyphicon glyphicon-trash"></span> <span class="icTextBtn">Удалить</span>
        </a>
    </div>



</div>


<?php Pjax::end(); ?>