<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


$this->title = "Статистика";

$this->registerJsFile('js/Chart.js');
$this->registerJs('
		var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
		var lineChartData = {
			labels : ["January","February","March","April","May","June","July"],
			datasets : [
				{
					label: "My First dataset",
					fillColor : "rgba(220,220,220,0.2)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
				},
				{
					label: "My Second dataset",
					fillColor : "rgba(151,187,205,0.2)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(151,187,205,1)",
					data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
				}
			]

		}

	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myLine = new Chart(ctx).Line(lineChartData, {
			responsive: true
		});
	}
	');


$this->registerCss("
.statistic {
	padding: 20px;
background: #FFF;
border: 1px solid #e0e0e0;
margin-bottom: 25px;
}
	");
?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>

<div class="contragent-view row">

    <h3><?= Html::encode($this->title) ?></h3>

		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="statistic">
				<div>
					<canvas id="canvas" height="450" width="600"></canvas>
				</div>
			</div>
		</div>
<div class="clear"></div>


</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
