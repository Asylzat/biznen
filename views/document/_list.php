<?php

use yii\helpers\Html;

$schetActive = "";
$schetHasData = false;
$schetClass = "mylist";

if ($model->schet != null) {
        if ($model->torg12 != null || $model->act != null || $model->schetFaktura != null) { $schetHasData = true; $schetClass = "mylistIn"; }
?>
<div class="mylist" onclick="selectRow(this, 'document')">
        <input type="hidden" value="schet" class="docType" />
        <input type="hidden" value='"id" : "<?= $model->id ?>", "type" : "schet", 
        "torg12" : "<?php if ($model->torg12 == null) echo "empty"; ?>", 
        "act" : "<?php if ($model->act == null) echo "empty"; ?>", 
        "schetFaktura" : "<?php if ($model->schetFaktura == null) echo "empty"; ?>", 
        "platejPoruch" : "<?php if ($model->platejPoruch == null) echo "empty"; ?>", 
        "upd" : "<?php if ($model->upd == null) echo "empty" ?>"' class="docId" />
        <input type="hidden" value="<?= $model->id ?>" class="id" />
        <input type="hidden" value="<?= $model->schet->id ?>" class="idself" />
        <div class="col-lg-1 col-md-2 col-sm-2"><?= $model->schet->date ?></div>
        <div class="col-lg-1 col-md-2 col-sm-2">
                <?php echo  $model->schet->id; if ($schetHasData) { ?>
                <a class="collapseBtS<?= $model->schet->id?>" role="button" data-toggle="collapse" href="#collapseFieldS<?= $model->schet->id?>" 
                        aria-expanded="false" aria-controls="collapseFieldS<?= $model->schet->id?>">
                      <span class="icon-border icon-blue glyphicon glyphicon-menu-down"></span> 
                </a> 
                <?php } ?>
        </div>
        <div class="col-lg-1 col-md-2 col-sm-2">Счет</div>
        <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->prodaves->name_contr ?></div>
        <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->pay_for ?></div>
        <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->prodaves->gen_director ?></div>
        <div class="col-lg-1 col-md-2 col-sm-2"><?= $model->schet->sum ?></div>
</div>


<?php
if ($schetHasData) {
        $this->registerJs('

                $("#collapseFieldS'.$model->schet->id.'").on("hidden.bs.collapse", function () {
                        $(".collapseBtS'.$model->schet->id.'").find("span").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
                });

                $("#collapseFieldS'.$model->schet->id.'").on("shown.bs.collapse", function () {
                        $(".collapseBtS'.$model->schet->id.'").find("span").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
                });

        ');

}

if ($schetHasData) echo '<div class="collapse"  id="collapseFieldS'.$model->schet->id.'">';
} 

if ($model->torg12 != null) {
?>

<div class="<?=$schetClass?>" onclick="selectRow(this, 'document')">
                <input type="hidden" value="torg12" class="docType" />
                <input type="hidden" value="<?= $model->id ?>" class="id" />
                <input type="hidden" value="<?= $model->torg12->id ?>" class="idself" />
                <div class="col-lg-1 col-md-2 col-sm-2"><?= $model->torg12->date ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2"><?=$model->torg12->id ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2">Торг 12</div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->prodaves->name_contr ?></div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->pay_for ?></div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->prodaves->gen_director ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2"><?= $model->torg12->sum ?></div>
</div>
<?php } 

if ($model->act != null) {
?>
<div class="<?=$schetClass?>" onclick="selectRow(this, 'document')">
                <input type="hidden" value="act" class="docType" />
                <input type="hidden" value="<?= $model->id ?>" class="id" />
                <input type="hidden" value="<?= $model->act->id ?>" class="idself" />
                <div class="col-lg-1 col-md-2 col-sm-2"><?= $model->act->date ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2"><?=$model->act->id ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2">Акт</div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->prodaves->name_contr ?></div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->pay_for ?></div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->prodaves->gen_director ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2"><?= $model->act->sum ?></div>
</div>
<?php } 

if ($model->schetFaktura != null) {
?>
<div class="<?=$schetClass?>" onclick="selectRow(this, 'document')">
                <input type="hidden" value="schetFaktura" class="docType" />
                <input type="hidden" value="<?= $model->id ?>" class="id" />
                <input type="hidden" value="<?= $model->schetFaktura->id ?>" class="idself" />
                <div class="col-lg-1 col-md-2 col-sm-2"><?= $model->schetFaktura->date ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2"><?=$model->schetFaktura->id ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2">Счет фактура</div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->prodaves->name_contr ?></div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->pay_for ?></div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->prodaves->gen_director ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2"><?= $model->schetFaktura->sum ?></div>
</div>
<?php } 

if ($model->platejPoruch != null) {
?>
<div class="<?=$schetClass?>" onclick="selectRow(this, 'document')">
                <input type="hidden" value="platejPoruch" class="docType" />
                <input type="hidden" value="<?= $model->id ?>" class="id" />
                <input type="hidden" value="<?= $model->platejPoruch->id ?>" class="idself" />
                <div class="col-lg-1 col-md-2 col-sm-2"><?= $model->platejPoruch->date ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2"><?=$model->platejPoruch->id ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2">Платежное поручение</div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->prodaves->name_contr ?></div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->pay_for ?></div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->prodaves->gen_director ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2"><?= $model->platejPoruch->sum ?></div>
</div>
<?php } 

if ($model->upd != null) {
?>
<div class="<?=$schetClass?>" onclick="selectRow(this, 'document')">
                <input type="hidden" value="upd" class="docType" />
                <input type="hidden" value="<?= $model->id ?>" class="id" />
                <input type="hidden" value="<?= $model->upd->id ?>" class="idself" />
                <div class="col-lg-1 col-md-2 col-sm-2"><?= $model->upd->date ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2"><?=$model->upd->id ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2">УПД</div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->prodaves->name_contr ?></div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->pay_for ?></div>
                <div class="col-lg-2 col-md-4 col-sm-4"><?= $model->prodaves->gen_director ?></div>
                <div class="col-lg-1 col-md-2 col-sm-2"><?= $model->upd->sum ?></div>
</div>
<?php } 

if ($schetHasData) echo '</div>';
?>


