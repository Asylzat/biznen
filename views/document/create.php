<?php

use yii\helpers\Html;

$this->title = 'Create Document';
$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>

<div class="document-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>


<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
