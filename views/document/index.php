<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

$this->title = 'Документооборот';

$this->registerJs("
$('#documentModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var id = button.attr('data-whatever');

  var modal = $(this);

  $('.withSchet').html('');

  if (id != ''){
    var obj = jQuery.parseJSON('{'+id+'}');
    if (obj.type == 'schet') {
      var label = '';
        
        label += '<div class=\"radio\"><label>'+
        '<input type=\"radio\" name=\"optionsRadios\" id=\"optionsRadios1\" value=\"option1\" checked >'+
        'Новый</label></div>';

      if (obj.torg12 == 'empty') label += '<div class=\"radio\"><label>'+
        '<input type=\"radio\" name=\"optionsRadios\" id=\"optionsRadios1\" value=\"option1\" docId=\"'+obj.id+'\" docType=\"torg12\">'+
        'Привязать Торг-12 к выбранному счету - '+obj.id+'</label></div>';

      if (obj.act == 'empty') label += '<div class=\"radio\"><label>'+
        '<input type=\"radio\" name=\"optionsRadios\" id=\"optionsRadios1\" value=\"option1\" docId=\"'+obj.id+'\" docType=\"act\">'+
        'Привязать Акт к выбранному счету - '+obj.id+'</label></div>';

      if (obj.schetFaktura == 'empty') label += '<div class=\"radio\"><label>'+
        '<input type=\"radio\" name=\"optionsRadios\" id=\"optionsRadios1\" value=\"option1\" docId=\"'+obj.id+'\" docType=\"schet-faktura\">'+
        'Привязать Счет фактуру к выбранному счету - '+obj.id+'</label></div>';

      if (obj.platejPoruch == 'empty') label += '<div class=\"radio\"><label>'+
        '<input type=\"radio\" name=\"optionsRadios\" id=\"optionsRadios1\" value=\"option1\" docId=\"'+obj.id+'\" docType=\"platej-poruch\">'+
        'Привязать Платежное поручение к выбранному счету - '+obj.id+'</label></div>';

      if (obj.upd == 'empty') label += '<div class=\"radio\"><label>'+
        '<input type=\"radio\" name=\"optionsRadios\" id=\"optionsRadios1\" value=\"option1\" docId=\"'+obj.id+'\" docType=\"upd\">'+
        'Привязать УПД к выбранному счету - '+obj.id+'</label></div>';
      
      $('.withSchet').html(label);
    }
  }
  //modal.find('.print').attr('href', '/schet/file?id='+id+'&action=print');
  modal.find('.save').attr('href', '/schet/file?id='+id+'&action=save&doctype=');
});


");

$this->registerJs("
$('#schetModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var id = $('.printBig').attr('data-whatever');

  var modal = $(this)
  modal.find('.print').attr('href', id+'&action=print');
  modal.find('.save').attr('href',  id+'&action=save&doctype=');
});


$('#schetModalEmail').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);

  var id = $('.saveBig').attr('data-whatever');
  var modal = $(this)

  modal.find('.email').attr('href', id+'&action=email&email=');

  //modal.find('.modal-body input').val(id);
});
");

$this->registerCss('
#documentModal .modal-header, #schetModal .modal-header, #schetModalEmail .modal-header {
  border-bottom:0px;
}

.backTo {
  float: initial;
  margin: 0px;
}

.modal-footer {
  border:0px;
}

@media screen and (min-width: 1000px) {
  .modal-lg {
    width: 900px;
  }
}
  ');
?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>


<div class="document-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>


    <?php Pjax::begin(['id'=>'catin']); ?>

        <div class="row myrow down">
            <div class="mylisth">
                <div class="col-md-1">Дата</div>
                <div class="col-md-1">Номер</div>
                <div class="col-md-1">Документ</div>
                <div class="col-md-2">Название компании</div>
                <div class="col-md-2">Оплата за ...</div>
                <div class="col-md-2">Продавец</div>
                <div class="col-md-2">Сумма</div>

            </div>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_list.php',
                'summary' => '',
                'viewParams' => [
                    'fullView' => true,
                ],
                'emptyText' => 'Список пуст',
            ]); 

            ?>
        </div>


    <?php Pjax::end(); ?>

</div>


<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>


<div class="modal fade" id="documentModal" tabindex="-1" role="dialog" aria-labelledby="documentModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="documentModalLabel">Создать</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <span class="glyphicon glyphicon-print"></span> Выберите тип документа
          </div>

          <div class="withSchet checkbox"></div>
          
          <div class="form-group">
            <select id="document-doc_type" class="form-control">
              <option value="schet">Счет</option>
              <option value="torg12">Торг12</option>
              <option value="act">Акт</option>
              <option value="schet-faktura">Счет фактура</option>
              <option value="platej-poruch">Платежное поручение</option>
              <option value="upd">УПД</option>
            </select>
            <div class="help-block"></div>
          </div>
        </form>
      </div>
      <div class="modal-footer backTo">
        <a type="button" class="btn btn-info save" onclick="return validateField(this, '#document-doc_type', 1, 30, false, 2)"><span class="glyphicon glyphicon-floppy-disk"></span> Новый</a>
        <button type="button" class="btn btn-info" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Отмена</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="schetModal" tabindex="-1" role="dialog" aria-labelledby="schetModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="schetModalLabel">Печать документов</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <span class="glyphicon glyphicon-print"></span> Выберите документы для печати
          </div>
          <div class="form-group">
            <select id="schet-doc_type" class="form-control">
              <option value="pdf">PDF</option>
              <option value="excel">EXCEL</option>
            </select>
            <div class="help-block"></div>
          </div>
        </form>
      </div>
      <div class="modal-footer backTo">
        <a type="button" class="btn btn-info print" target="_blank"><span class="glyphicon glyphicon-print"></span> Напечатать</a>
        <a type="button" class="btn btn-info save" onclick="return validateField(this, '#schet-doc_type', 1, 30, false, 1)"><span class="glyphicon glyphicon-floppy-disk"></span> Сохранить</a>
        <button type="button" class="btn btn-info" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Отмена</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="schetModalEmail" tabindex="-1" role="dialog" aria-labelledby="schetModalLabelEmail">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="schetModalLabelEmail">Отправить e-mail</h4>
      </div>
      <div class="modal-body">
        <form class="row">
          <div class="col-lg-6 col-md-6 col-sm-12">
            <span class="glyphicon glyphicon-print"></span> Выберите документы для печати

          </div>
          <div class="col-lg-6 col-md-6 col-sm-12">
            <span class="glyphicon glyphicon-print"></span> Кому
            
            <div class="radio">
              <label><input name="optradio" type="radio" id="optradio" data-toggle="collapse" data-target=".collapseOne.in" checked>
                 <span class="glyphicon glyphicon-user"></span> Себе <?=Yii::$app->user->identity->email?></label>
            </div>
            <div class="radio">
              <label><input name="optradio" type="radio" id="optradio2" data-toggle="collapse" data-target=".collapseOne:not(.in)"> Ввести другой e-mail</label>
            </div>


            <div class="form-group required collapseOne panel-collapse collapse">
              <input id="schet-email" class="form-control" type="text">
              <div class="help-block"></div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer backTo">
        <a type="button" class="btn btn-info email" onclick="return newValidateField(this, '#schet-email', 1, 30, true, 1)" style="float:left"><span class="glyphicon glyphicon-print"></span> Отправить</a>

        <button type="button" class="btn btn-info" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Отмена</button>
      </div>
    </div>
  </div>
</div>

