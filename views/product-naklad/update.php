<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductNaklad */

$this->title = 'Update Product Naklad: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Naklads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>
<div class="product-naklad-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
