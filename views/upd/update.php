<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Upd */

$this->title = 'УПД Редактировать - ' . $model->id;
?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>
<div class="upd-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'document' => $document,
        'docId' => $docId,
    ]) ?>

</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
