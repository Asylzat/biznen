<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->registerJs('
    $(document).ready(function(){
        $("#catout").on("pjax:end", function(){
            $.pjax.reload({ container : "#catin" });
        });

        $(".delete").click(function(event){
            if ($(this).attr("href") == "" || $(this).attr("href") == "#") return false;
            else $(this).attr("data-confirm", "Вы уверенны что хотите удалить?")
        });

    });

    ');
?>

<?php Pjax::begin(['id' => 'catout']); ?>

<div class="row myrow">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options' => ['data-pjax'=>true, 'onchange' => "var container = $(this).closest('[data-pjax-container]');    $.pjax.submit(event, container)"],
        ]); ?>


        <?= $form->field($model, 'innNameSearch')->textInput(['autofocus' => true, 'placeholder' => 'Введите наименование или ИНН']) ?>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4 margint20">
        <?= Html::submitButton('<span class="glyphicon glyphicon-search"></span> <span class="icTextBtn">Поиск</span> ', ['class' => 'btn linkBtn']) ?>
    </div>
    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4 margint20">
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> <span class="icTextBtn">Добавить контрагента</span>', ['create'], ['class' => 'btn linkBtn']) ?>
    </div>


</div>

<div class="row myrow up">
    <div class="col-lg-2 col-md-4 col-sm-4">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options' => ['data-pjax'=>true, 'onchange' => "var container = $(this).closest('[data-pjax-container]');    $.pjax.submit(event, container)"],
        ]); ?>


        <?= $form->field($model, 'name_contr')->textInput(['autofocus' => true, 'placeholder' => 'поиск...'])->label(false) ?>

        <?php ActiveForm::end(); ?>

    </div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> <span class="icTextBtn">Редактировать</span>', ['#'], ['class' => 'btn linkBtn edit']) ?>
    </div>
    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
        <a href="#" class="btn linkBtn delete" title="Delete" aria-label="Delete" data-method="post" data-pjax="0">
            <span class="glyphicon glyphicon-trash"></span> <span class="icTextBtn">Удалить</span>
        </a>
    </div>

</div>


<?php Pjax::end(); ?>