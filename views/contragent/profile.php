<?php

use yii\helpers\Html;
use yii\grid\GridView;


$this->title = 'Добро пожаловать!';

?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>

<div class="delivery-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-12 col-xs-12 col-lg-5 col-md-5 col-sm-12">

            <div class="member-home-mkg">
                <div class="mkg-inner">
                    <h3><span class="glyphicon glyphicon-user"></span> Мои персональные данные</h3>
                    <p>Наименование контрагента : <?= Yii::$app->user->identity->kpp ?></p>
                    <p>КПП : <?= Yii::$app->user->identity->kpp ?></p>
                    <p>ОГРН : <?= Yii::$app->user->identity->ogrn ?></p>
                    <p>Генеральный директор (Ген. директор) : <?= Yii::$app->user->identity->gen_director ?></p>
                    <p>Юридический адрес : <?= Yii::$app->user->identity->ur_adress ?></p>
                    <p>Фактический адрес : <?= Yii::$app->user->identity->fak_adress ?></p>
                    <p>Телефон (тел.) : <?= Yii::$app->user->identity->phone ?></p>
                    <p><a href="/user/personal?id=<?= Yii::$app->user->identity->id ?>"><span class="glyphicon glyphicon-wrench"></span> Редактировать</a></p>
                    <p><a href="/user/password?id=<?= Yii::$app->user->identity->id ?>"><span class="glyphicon glyphicon-wrench"></span> Изменить пароль</a></p>
                </div>
            </div>
            <div class="member-home-mkg">
                <div class="mkg-inner">
                    <h3><span class="glyphicon glyphicon-envelope"></span> Мой почтовый адрес</h3>
                    <p>Емайл : <?= Yii::$app->user->identity->email ?></p>
                    <p><a href="/user/email?id=<?= Yii::$app->user->identity->id ?>"><span class="glyphicon glyphicon-wrench"></span> Редактировать</a></p>
                </div>
            </div>

            <?php if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['admin'])) { ?>
                <div class="member-home-mkg">
                    <div class="mkg-inner">
                        <h3><span class="glyphicon glyphicon-list"></span> Зарегистрированные пользователи</h3>
                        <p>Всего : <?= \app\models\User::find()->count() ?> зарегистрированных пользователей</p>
                        <p><a href="/user"><span class="glyphicon glyphicon-eye-open"></span> Просмотреть всех</a></p>
                    </div>
                </div>

            <?php } ?>
        </div>

        <div class="col-12 col-xs-12 col-lg-4 col-md-4 col-sm-12">
            <div class="member-home-mkg">
                <div class="mkg-inner">
                    <h3><a href="/balance"><span class="glyphicon glyphicon-usd"></span> Баланс <?= Yii::$app->user->identity->money ?> РУБ</a></h3>
                    <h4><a href="/mtransfer/alltransfer"><span class="glyphicon glyphicon-book"></span> Показать все чеки</a></h4>
                </div>
            </div>

            <?php if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['admin'])) { ?>
                <div class="member-home-mkg">
                    <div class="mkg-inner">
                        <h4><a href="mtransfer"><span class="glyphicon glyphicon-piggy-bank"></span> Снять или положить деньги на счет пользователей</a></h4>
                    </div>
                </div>

            <?php } ?>


        </div>



    </div>

</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
