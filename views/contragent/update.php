<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contragent */

$this->title = 'Редактирование контрагента : ' . $model->id;
?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>

<div class="contragent-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
