<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ListView;


$this->title = 'Контрагенты';
?>


<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>


        <div class="contragent-index">

            <h3><?= Html::encode($this->title) ?></h3>
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>


            <?php Pjax::begin(['id'=>'catin']); ?>

                <div class="row myrow down">
                    <div class="mylisth">
                        <div class="col-lg-1 col-md-2 col-sm-2">Дата</div>
                        <div class="col-lg-1 col-md-2 col-sm-2">Номер</div>
                        <div class="col-lg-1 col-md-2 col-sm-2">ИНН</div>
                        <div class="col-lg-2 col-md-3 col-sm-3">Название компании</div>
                        <div class="col-lg-7 col-md-3 col-sm-3">Комментарий</div>
                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => '_list.php',
                        'summary' => '',
                        'viewParams' => [
                            'fullView' => true,
                        ],
                        'emptyText' => 'Список пуст',
                    ]); 

                    ?>
                </div>


            <?php Pjax::end(); ?>
        </div>


<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>







