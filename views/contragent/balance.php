<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Delivery */

$this->title = 'Выбор тарифного плана';

echo \Yii::$app->view->renderFile('@app/views/site/userheader.php');

?>


<div class="balance-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <div class="row balance">
    	<div class="col-md-2 myrow myrow1">
    		<div class="balanceHeader"><span>ВАШ ИНН №:</span><span style="float: right;"><?= Yii::$app->user->identity->username ?></span></div>
    		<div><span class="icomoon icomoon-banknote balanceIcon"></span><span class="balanceMoney">ВАШ БАЛАНС<br><span style="font-size:26px;color:#FFF;"><?= Yii::$app->user->identity->money ?></span></span></div>
    	</div>
    	<div class="col-md-10">
			<div class="row myrow">
				<form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
				    <input type="hidden" name="receiver" value="410012029706877">
				    <input type="hidden" name="formcomment" value="Biznen : Пополнение баланса, пользователь <?= Yii::$app->user->identity->username ?>">
				    <input type="hidden" name="short-dest" value="Biznen : Пополнение баланса, пользователь <?= Yii::$app->user->identity->username ?>">
				    <input type="hidden" name="label" value="<?= Yii::$app->user->identity->id ?>">
				    <input type="hidden" name="quickpay-form" value="shop">
				    <input type="hidden" name="targets" value="Biznen : Пополнение баланса, пользователь <?= Yii::$app->user->identity->username ?>">
				    <div class="col-md-3">
						<label class="control-label">Сумма пополнения (руб.)</label>
						<input class="form-control" name="sum" type="text" data-type="number" id="pay_Sum">
				    </div>
				    <div class="col-md-2">
							<label class="control-label" for="category-page_id">Продавец</label>
							<select id="category-page_id" class="form-control" name="paymentType">
								<option value="PC">Яндекс Деньгами</option>
								<option value="AC">Банковской картой</option>
							</select>

				    </div>
				    <div class="col-md-1 margint20 backTo">
						<button type="submit" class="btn btn-info" onclick="return validateField(this, '#pay_Sum', 1, 30, false)"><span class="glyphicon glyphicon-plus"></span> Пополнить</button>  
					</div>
				</form>
			</div>
		</div>
	</div>


</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>