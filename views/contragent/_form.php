<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contragent */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile("/js/jquery.kladr.min.js", ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile("http://api-maps.yandex.ru/2.1/?lang=ru_RU", ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile("/css/jquery.kladr.min.css");

$this->registerJs('
var changeInn = false;

$("#contragent-inn").change(function(){
	if ($("#contragent-inn").val().length == 10) {
		changeInn = true;
		$(".parse").removeClass("parseError");
	} else $(".parse").addClass("parseError");
});

$(".parse").click(function (){
	if (!$(".parse").hasClass("parseError") && changeInn){
		changeInn = false;
		$(".parse").addClass("parseError");
		var findValueF = "";
		if ($("#contragent-inn").val() != ""){
			findValueF = $("#contragent-inn").value;
		}
		
		//findValueF = "4345265460";
		
		if (findValueF != ""){
			$.ajax({
			  method: "GET",
			  url: "/cgi-bin/index.py",
			  data: { findValue : findValueF }
			})
			  .done(function( msg ) {
			  	msg = msg.trim();
			  	if (msg != ""){

				  	console.log(msg);
				  	var obj = jQuery.parseJSON( msg );
				  	if ($("#contragent-name_contr_full").val() == "") $("#contragent-name_contr_full").val(obj.field1);
				    //alert( "Data Saved: " + msg + obj.field1 );
				} else {
					console.log("parsing error");
				}
			  });
		}

	}

});

//0
$("#collapseField0").on("hidden.bs.collapse", function () {
  $(".collapseBt0").find("span").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
});

$("#collapseField0").on("shown.bs.collapse", function () {
  $(".collapseBt0").find("span").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
});


//1
$("#collapseField1").on("hidden.bs.collapse", function () {
  $(".collapseBt1").find("span").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
});

$("#collapseField1").on("shown.bs.collapse", function () {
  $(".collapseBt1").find("span").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
});


//2
$("#collapseField2").on("hidden.bs.collapse", function () {
  $(".collapseBt2").find("span").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
});

$("#collapseField2").on("shown.bs.collapse", function () {
  $(".collapseBt2").find("span").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
});


//3
$("#collapseField3").on("hidden.bs.collapse", function () {
  $(".collapseBt3").find("span").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
});

$("#collapseField3").on("shown.bs.collapse", function () {
  $(".collapseBt3").find("span").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
});

//4
$("#collapseField4").on("hidden.bs.collapse", function () {
  $(".collapseBt4").find("span").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
});

$("#collapseField4").on("shown.bs.collapse", function () {
  $(".collapseBt4").find("span").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
});

//5
$("#collapseField5").on("hidden.bs.collapse", function () {
  $(".collapseBt5").find("span").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
});

$("#collapseField5").on("shown.bs.collapse", function () {
  $(".collapseBt5").find("span").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
});

	');

$this->registerJs('
$(function () {

	// One string example
	(function () {
		var $container = $(\'.field-contragent-ur_adress\');

		var $address = $container.find(\'[name=\"Contragent[ur_adress]\"]\');

		$address.kladr({
			oneString: true,
			change: function (obj) {
				log(obj);
			}
		});

		function log(obj) {
			var $log, i;

			$container.find(\'.js-log li\').hide();

			for (i in obj) {
				$log = $container.find(\'[data-prop=\"\' + i + \'\"]\');

				if ($log.length) {
					$log.find(\'.value\').text(obj[i]);
					$log.show();
				}
			}
		}
	})();

	// One string example
	(function () {
		var $container = $(\'.field-contragent-fak_adress\');

		var $address = $container.find(\'[name=\"Contragent[fak_adress]\"]\');

		$address.kladr({
			oneString: true,
			change: function (obj) {
				log(obj);
			}
		});

		function log(obj) {
			var $log, i;

			$container.find(\'.js-log li\').hide();

			for (i in obj) {
				$log = $container.find(\'[data-prop=\"\' + i + \'\"]\');

				if ($log.length) {
					$log.find(\'.value\').text(obj[i]);
					$log.show();
				}
			}
		}
	})();

	// One string example
	(function () {
		var $container = $(\'.field-contragent-poch_adress\');

		var $address = $container.find(\'[name=\"Contragent[poch_adress]\"]\');

		$address.kladr({
			oneString: true,
			change: function (obj) {
				log(obj);
			}
		});

		function log(obj) {
			var $log, i;

			$container.find(\'.js-log li\').hide();

			for (i in obj) {
				$log = $container.find(\'[data-prop=\"\' + i + \'\"]\');

				if ($log.length) {
					$log.find(\'.value\').text(obj[i]);
					$log.show();
				}
			}
		}
	})();	
	
});

	');


/***************************  bank get editable data  **********************/

$script = '';
$script2 = '';

foreach($model->banks as $current){
   $script .= 'bank.addService("' . $current->number . '", "' . $current->id . '", "' . $current->bik . '", "' . $current->name . '", 
    "' . $current->kor_schet . '", "' . $current->ras_schet . '");';
}

if ($script == '') $script .= 'bank.addService();'; 

foreach($model->informs as $current){
   $script2 .= 'contactInform.addService("' . $current->number . '", "' . $current->id . '", "' . $current->fio . '", "' . $current->email . '", 
    "' . $current->inform . '");';
}

if ($script2 == '') $script2 .= 'contactInform.addService();'; 

$this->registerJsFile('/js/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/Bank.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJs($script . $script2);

/***************************  bank get editable data  **********************/

$this->registerCss("
body .myrow.down.greyTable .mylisth, body .myrow.down.greyTable .mylistf {
    background: #FFF;
}

body .myrow.down.greyTable{
	margin-top: 14px;
}
");
?>


<div class="contragent-form">

    <?php $form = ActiveForm::begin(); ?>

		<div class="row myrow">
			<div class="col-lg-2 col-md-3 col-sm-4" style="margin-top: 6px;">
		        <?= Html::submitButton($model->isNewRecord ? '<span class="glyphicon glyphicon-floppy-disk"></span> <span class="icTextBtn">Сохранить</span>' : 
		        '<span class="glyphicon glyphicon-pencil"></span> <span class="icTextBtn">Редактировать</span>', ['class' => $model->isNewRecord ? 'linkBtn' : 'linkBtn']) ?>
			</div>
			<?php if (!$model->isNewRecord){ ?>
                <div class="col-lg-1 col-md-2 col-sm-4 col-xs-4">
                    <a href="/contragent/delete?id=<?=$model->id?>" class="btn linkBtn delete" title="Delete" aria-label="Delete" data-method="post" data-pjax="0">
                        <span class="glyphicon glyphicon-trash"></span> <span class="icTextBtn">Удалить</span>
                    </a>
                </div>
                <div class="col-lg-1 col-md-2 col-sm-4 col-xs-4">
                    <?= Html::a('<span class="glyphicon glyphicon-plus-sign"></span> <span class="icTextBtn">Создать еще</span>', 
                    ['/contragent/create'], ['class' => 'btn linkBtn']) ?>
                </div>
			<?php } ?>
		</div>

		<div class="row myrow">
			<div class="col-lg-3 col-md-3 col-sm-6">
			    <?= $form->field($model, 'inn')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6">
			    <?= $form->field($model, 'kpp')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6">
			    <?= $form->field($model, 'ogrn')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6">
			    <?= $form->field($model, 'okpo')->textInput(['maxlength' => true]) ?>
			</div>
		</div>

		<div>
			<a class="btn my-btn parse parseError"><span class="glyphicon glyphicon-edit"></span> ЗАПОЛНИТЬ ПО ИНН/ОГРН</a>
		</div>


		<div class="row myrow">
			<div class="col-lg-3 col-md-3 col-sm-6">
			    <?= $form->field($model, 'name_contr')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-6">
			    <?= $form->field($model, 'name_contr_full')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6">
			    <?= $form->field($model, 'svidet_reg_ip')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-6">
			    <?= $form->field($model, 'pasport')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6">
			    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6">
			    <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6">
			    <?= $form->field($model, 'group')->textInput(['maxlength' => true]) ?>
			</div>
		</div>



		<div class="collapseBtF">
			<a class="collapseBt0" role="button" data-toggle="collapse" href="#collapseField0" aria-expanded="false" aria-controls="collapseField0">
			  <span class="icon-border icon-blue glyphicon glyphicon-menu-up"></span> 
			</a> 
			<span class="icomoon icomoon-books"></span> <span class="icText">Данные для документов</span>
		</div>
		<div class="row myrow collapse in"  id="collapseField0">
			<div class="col-lg-3 col-md-3 col-sm-6">
			    <?= $form->field($model, 'gen_director')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6">
			    <?= $form->field($model, 'glav_buhgalter')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6">
			    <?= $form->field($model, 'in_predprinimatel')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
			    <?= $form->field($model, 'preambula1_2')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
			    <?= $form->field($model, 'preambula2_2')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
			    <?= $form->field($model, 'uslov_name_pokup')->textInput(['maxlength' => true]) ?>
			</div>
		</div>



		<div class="collapseBtF">
			<a class="collapseBt1" role="button" data-toggle="collapse" href="#collapseField1" aria-expanded="false" aria-controls="collapseField1">
			  <span class="icon-border icon-blue glyphicon glyphicon-menu-up"></span> 
			</a> 
			<span class="icomoon icomoon-diary"></span> <span class="icText">Налоговая ставка и валюта</span>
		</div>
		<div class="row myrow collapse in"  id="collapseField1">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <?= $form->field($model, 'nds')->dropDownList(
                    ['18' => 'в т. ч. НДС 18%', '0' => 'без НДС', '10' => 'в т. ч. НДС 10%'],
                    ['prompt'=>'Ставка НДС', 'autofocus' => true]
                ) ?>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6">
                <?= $form->field($model, 'currency')->dropDownList(
                    ['643' => 'Российский рубль, 643', '840' => 'Доллар США, 840', '978' => 'Евро, 978',
                    '974' => 'Белорусский рубль, 974', '980' => 'Гривна, 980', '398' => 'Тенге, 398' ],
                    ['prompt'=>'Наименование, код', 'autofocus' => true]
                ) ?>
            </div>
		</div>


		<div class="collapseBtF">
			<a class="collapseBt2" role="button" data-toggle="collapse" href="#collapseField2" aria-expanded="false" aria-controls="collapseField2">
			  <span class="icon-border icon-blue glyphicon glyphicon-menu-up"></span> 
			</a> 
			<span class="icomoon icomoon-location2"></span> <span class="icText">Адреса</span>
		</div>
		<div class="row myrow collapse in"  id="collapseField2">
			<div class="col-lg-12 col-md-12 col-sm-12">
			    <?= $form->field($model, 'ur_adress')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
			    <?= $form->field($model, 'fak_adress')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
			    <?= $form->field($model, 'poch_adress')->textInput(['maxlength' => true]) ?>
			</div>
		</div>
        
		<div class="collapseBtF">
			<a class="collapseBt3" role="button" data-toggle="collapse" href="#collapseField3" aria-expanded="false" aria-controls="collapseField3">
			  <span class="icon-border icon-blue glyphicon glyphicon-menu-up"></span> 
			</a> 
			<span class="icomoon icomoon-library"></span> <span class="icText">Банковские реквизиты</span>
		</div>
		<div class="collapse in"  id="collapseField3">
        <?php 
        
        /***************************  Service get editable data  **********************/
        echo \Yii::$app->view->renderFile('@app/views/contragent-bank/_bank.php'); 

        ?>
		</div>

		<div class="collapseBtF">
			<a class="collapseBt4" role="button" data-toggle="collapse" href="#collapseField4" aria-expanded="false" aria-controls="collapseField4">
			  <span class="icon-border icon-blue glyphicon glyphicon-menu-up"></span> 
			</a> 
			<span class="icomoon icomoon-profile"></span> <span class="icText">Контактная информация</span>
		</div>
		<div class="collapse in"  id="collapseField4">
        <?php 
        
        /***************************  Service get editable data  **********************/
        echo \Yii::$app->view->renderFile('@app/views/contact-inform/_inform.php'); 

        ?>
		</div>

		<div class="collapseBtF">
			<a class="collapseBt5" role="button" data-toggle="collapse" href="#collapseField5" aria-expanded="false" aria-controls="collapseField5">
			  <span class="icon-border icon-blue glyphicon glyphicon-menu-up"></span> 
			</a> 
			<span class="icomoon icomoon-display"></span> <span class="icText">Дополнительная информация</span>
		</div>
		<div class="row myrow collapse in"  id="collapseField5">
			<div class="col-lg-12 col-md-12 col-sm-12">
			    <?= $form->field($model, 'dop_inform')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
			    <?= $form->field($model, 'about')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
			    <?= $form->field($model, 'zametka')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
			    <?= $form->field($model, 'dop_code')->textInput(['maxlength' => true]) ?>
			</div>
		</div>



        <span class="backTo">
        	<?= Html::submitButton($model->isNewRecord ? '<span class="glyphicon glyphicon-floppy-disk"></span> <span class="icTextBtn">Сохранить</span>' : 
        	'<span class="glyphicon glyphicon-pencil"></span> <span class="icTextBtn">Редактировать</span>', ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-info']) ?>
        	<a href="/contragent" class="collapseBtF" style="position: relative;top: 5px;">
        		<span class="icomoon icomoon-mail-reply"></span> <span class="icText">Назад к Контрагентам</span>
        	</a>
        </span>
    <?php ActiveForm::end(); ?>

</div>
