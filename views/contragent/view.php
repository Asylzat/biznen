<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contragent */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Contragents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>

<div class="contragent-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name_contr',
            'name_contr_full',
            'svidet_reg_ip',
        ],
    ]) ?>

</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
