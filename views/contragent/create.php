<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Заведение контрагента';

?>


<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>


<div class="contragent-create">


    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>


<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>