<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ContactInform */

$this->title = 'Create Contact Inform';
$this->params['breadcrumbs'][] = ['label' => 'Contact Informs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-inform-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
