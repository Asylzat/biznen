<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactInformSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contact Informs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-inform-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Contact Inform', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fio',
            'email:email',
            'inform',
            'contragent_id',
            // 'number',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
