<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ContactInform */

$this->title = 'Update Contact Inform: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Contact Informs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contact-inform-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
