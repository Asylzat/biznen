<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use app\models\Contragent;

$this->registerJs("
//0
$('#collapseField0').on('hidden.bs.collapse', function () {
  $('.collapseBt0').find('span').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
});

$('#collapseField0').on('shown.bs.collapse', function () {
  $('.collapseBt0').find('span').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
});


//1
$('#collapseField1').on('hidden.bs.collapse', function () {
  $('.collapseBt1').find('span').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
});

$('#collapseField1').on('shown.bs.collapse', function () {
  $('.collapseBt1').find('span').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
});

$('#date input').datepicker({ autoclose: true, format: 'mm.dd.yyyy', });

    ");

/***************************  Service get editable data  **********************/

$script = '';

foreach($model->services as $current){
   $script .= 'service.addService("' . $current->number . '", "' . $current->id . '", "' . $current->title . '", "' . $current->prize . '", 
    "' . $current->count . '", "' . $current->code . '", "' . $current->code_name . '", "' . $current->nds . '", "' . $current->sum . '");';
}

if ($script == '') $script .= 'service.addService();'; 

$this->registerJsFile('/js/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/Service.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJs($script . 'service.reCount();');

/***************************  Service get editable data  **********************/



$this->registerJsFile('/js/bootstrap-datepicker.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile('/css/bootstrap-datepicker.css');


$this->registerCss('
.collapseBtF {
    margin-bottom:20px;
}
    ');
if ($document->schet !== null) echo "<h3> Привязать к счету - " . $document->schet->id . "</h3>";

?>

<div class="torg12-form">

    <?php $form = ActiveForm::begin(); ?>
        <div class="row myrow">
            <div class="col-lg-2 col-md-2 col-sm-4">
                <?= $form->field($model, 'idnum')->textInput(['maxlength' => true, 'autofocus' => true, 'placeholder' => '0001']) ?>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-4" id="date">
                <?= $form->field($model, 'date')->textInput(['maxlength' => true, 'autofocus' => true, 'placeholder' => 'dd.mm.yyyy']) ?>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 margint20" style="margin-top: 26px;">
                <?= Html::submitButton($model->isNewRecord ? 
                    '<span class="glyphicon glyphicon-floppy-disk"></span> <span class="icTextBtn">Сохранить</span>' : 
                    '<span class="glyphicon glyphicon-pencil"></span> <span class="icTextBtn">Редактировать</span>', 
                    ['class' => $model->isNewRecord ? 'linkBtn' : 'linkBtn']) ?>
            </div>
            <?php if (!$model->isNewRecord) { ?>
                <div class="col-lg-1 col-md-2 col-sm-4 col-xs-4 margint20">
                    <?= Html::a('<span class="glyphicon glyphicon-print"></span> <span class="icTextBtn">Печать</span>', 
                    ['/torg12/file?id='.$model->id.'&action=print'], ['class' => 'btn linkBtn print']) ?>
                </div>
                <div class="col-lg-1 col-md-2 col-sm-4 col-xs-4 margint20">
                    <?= Html::a('<span class="glyphicon glyphicon-floppy-disk"></span> <span class="icTextBtn">E-mail</span>', 
                    ['#'], ['class' => 'btn linkBtn save']) ?>
                </div>
                <div class="col-lg-1 col-md-2 col-sm-4 col-xs-4 margint20">
                    <a href="/torg12/delete?id=<?=$model->id?>" class="btn linkBtn delete" title="Delete" aria-label="Delete" data-method="post" data-pjax="0">
                        <span class="glyphicon glyphicon-trash"></span> <span class="icTextBtn">Удалить</span>
                    </a>
                </div>
            <?php } ?>
        </div>


        <?php if ($docId == null) { ?>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="row myrow">
                    <div class="col-md-12">
                        <?= $form->field($document, 'prodaves_id')->dropDownList(
                            ArrayHelper::map(Contragent::find()->where(['createdBy' => Yii::$app->user->getId()])->all(),'id','inn'),
                            ['prompt'=>'Выберите продавца', 'autofocus' => true]
                        ) ?>

                        <div class="collapseBtF">
                            <a class="collapseBt0" role="button" data-toggle="collapse" href="#collapseField0" aria-expanded="false" aria-controls="collapseField0">
                              <span class="icon-border icon-blue glyphicon glyphicon-menu-up"></span> 
                            </a> <span class="icText">Показать все реквизиты</span>
                        </div>

                    </div>
                    <div class="col-md-12 collapse in"  id="collapseField0">
                        <a class="btn my-btn" href="/contragent/create">
                            <span class="glyphicon glyphicon-plus"></span> <span class="icTextBtn">Добавить свою организацию</span>
                        </a>

                    </div>

                </div>

            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="row myrow">
                    <div class="col-md-12">
                        <?= $form->field($document, 'pokupatel_id')->dropDownList(
                            ArrayHelper::map(Contragent::find()->where(['createdBy' => Yii::$app->user->getId()])->all(),'id','inn'),
                            ['prompt'=>'Выберите покупателя', 'autofocus' => true]
                        ) ?>
                        <div class="collapseBtF">
                            <a class="collapseBt1" role="button" data-toggle="collapse" href="#collapseField1" aria-expanded="false" aria-controls="collapseField1">
                              <span class="icon-border icon-blue glyphicon glyphicon-menu-up"></span> 
                            </a> <span class="icText">Показать все реквизиты</span>
                        </div>
                    </div>
                    <div class="col-md-12 collapse in" id="collapseField1">
                        <a class="btn my-btn" href="/contragent/create">
                            <span class="glyphicon glyphicon-plus"></span> <span class="icTextBtn">Добавить нового контрагента</span>
                        </a>
                    </div>

                </div>

            </div>
        </div>





        <div class="row myrow">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <?= $form->field($document, 'pay_for')->textInput(['maxlength' => true, 'autofocus' => true, 'placeholder' => 'Пример: расходные материалы за аренду']) ?>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <?= $form->field($document, 'byDogovor')->textInput(['maxlength' => true, 'autofocus' => true, 'placeholder' => 'Пример: № АВ100/12 от 27 августа 2015 г.']) ?>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6">
                <?= $form->field($document, 'nds')->dropDownList(
                    ['18' => 'в т. ч. НДС 18%', '0' => 'без НДС', '10' => 'в т. ч. НДС 10%'],
                    ['prompt'=>'Ставка НДС', 'autofocus' => true]
                ) ?>
            </div>

            <div class="col-lg-2 col-md-4 col-sm-6">
                <?= $form->field($document, 'currency')->dropDownList(
                    ['643' => 'Российский рубль, 643', '840' => 'Доллар США, 840', '978' => 'Евро, 978',
                    '974' => 'Белорусский рубль, 974', '980' => 'Гривна, 980', '398' => 'Тенге, 398' ],
                    ['prompt'=>'Наименование, код', 'autofocus' => true]
                ) ?>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6">
                <?= $form->field($document, 'pechatforma')->dropDownList(
                    ['Счет', 'Счет (1С)', 'Счет ИП', 'Счет ИП (1С)', 'Счет (1С без лого)', 'Счет ИП (1С без лого)',
                    'Счет со скидкой', 'Счет со скидкой (1С)', 'Счет со скидкой (1С без лого)' ],
                    ['prompt'=>'Печатная форма', 'autofocus' => true]
                ) ?>
            </div>
        </div>

        <?php } ?>


        <?php 
        
        /***************************  Service get editable data  **********************/
        echo \Yii::$app->view->renderFile('@app/views/service/_service.php', [ 'model' => $model->sum ]); 

        ?>





        <span class="backTo">
            <?= Html::submitButton($model->isNewRecord ? '<span class="glyphicon glyphicon-floppy-disk"></span> <span class="icTextBtn">Сохранить</span>' : 
            '<span class="glyphicon glyphicon-pencil"></span> <span class="icTextBtn">Редактировать</span>', ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-info']) ?>
        </span>

    <?php ActiveForm::end(); ?>

</div>

























