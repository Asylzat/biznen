<?php

use yii\helpers\Html;

$this->title = 'Счет фактура';
?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>
<div class="torg12-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'document' => $document,
        'docId' => $docId,
    ]) ?>

</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
