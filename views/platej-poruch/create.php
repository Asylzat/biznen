<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PlatejPoruch */

$this->title = 'Платежное поручение';
?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>

<div class="platej-poruch-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'document' => $document,
        'docId' => $docId,
    ]) ?>

</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
