<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlatejPoruchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Platej Poruches';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>
<div class="platej-poruch-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Platej Poruch', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
