<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SchetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'История последних документов';

$this->registerJs("
$('#schetModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var id = button.data('whatever');

  var modal = $(this)
  modal.find('.print').attr('href', '/schet/file?id='+id+'&action=print');
  modal.find('.save').attr('href', '/schet/file?id='+id+'&action=save&doctype=');
});


$('#schetModalEmail').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var id = button.data('whatever');
  var modal = $(this)

  modal.find('.email').attr('href', '/schet/file?id='+id+'&action=email&email=');

  //modal.find('.modal-body input').val(id);
});
");

$this->registerCss('
#schetModal .modal-header, #schetModalEmail .modal-header {
  border-bottom:0px;
}

.backTo {
  float: initial;
  margin: 0px;
}

.modal-footer {
  border:0px;
}

@media screen and (min-width: 1000px) {
  .modal-lg {
    width: 900px;
  }
}
  ');
?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>
<div class="schet-index">

    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12">
            <h3><?= Html::encode($this->title) ?></h3>

            <div class="row myrow-schet">
                <div class="mylisth">
                    <div class="col-lg-1 col-md-1 col-sm-1"></div>
                    <div class="col-lg-3 col-md-3 col-sm-3">Порядковый номер</div>
                    <div class="col-lg-3 col-md-3 col-sm-3">№ документа с датой</div>
                    <div class="col-lg-3 col-md-3 col-sm-3">Дата создания</div>
                </div>
                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '_list.php',
                    'summary' => '',
                    'viewParams' => [
                        'fullView' => true,
                    ],
                    'emptyText' => 'Список пуст',
                ]); 

                ?>
            </div>

        </div>
        <div class="col-lg-6 col-md-12 col-sm-12">
            <h3>Краткая информация о компании</h3>

            <div class="row myrow-schet">
                <div class="mylisth">
                  <div class="col-lg-3 col-md-3 col-sm-3">Название:</div>
                  <div class="col-lg-9 col-md-9 col-sm-9"><?= Yii::$app->user->identity->name_contr ?></div>
                </div>
                <div class="mylist">
                  <div class="col-lg-3 col-md-3 col-sm-3">ИНН:</div>
                  <div class="col-lg-9 col-md-9 col-sm-9"><?= Yii::$app->user->identity->username ?></div>
                </div>
                <div class="mylist">
                  <div class="col-lg-3 col-md-3 col-sm-3">Адрес:</div>
                  <div class="col-lg-9 col-md-9 col-sm-9"><?= Yii::$app->user->identity->username ?></div>
                </div>
                <div class="mylist">
                  <div class="col-lg-3 col-md-3 col-sm-3">Телефон:</div>
                  <div class="col-lg-9 col-md-9 col-sm-9"><?= Yii::$app->user->identity->phone ?></div>
                </div>
                <div class="mylist">
                  <div class="col-lg-3 col-md-3 col-sm-3">Email:</div>
                  <div class="col-lg-9 col-md-9 col-sm-9"><?= Yii::$app->user->identity->email ?></div>
                </div>
            </div>

        </div>
    </div>

</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>


<div class="modal fade" id="schetModal" tabindex="-1" role="dialog" aria-labelledby="schetModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="schetModalLabel">Печать документов</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <span class="glyphicon glyphicon-print"></span> Выберите документы для печати
          </div>
          <div class="form-group">
            <select id="schet-doc_type" class="form-control">
              <option value="pdf">PDF</option>
              <option value="excel">EXCEL</option>
            </select>
            <div class="help-block"></div>
          </div>
        </form>
      </div>
      <div class="modal-footer backTo">
        <a type="button" class="btn btn-info print" target="_blank"><span class="glyphicon glyphicon-print"></span> Напечатать</a>
        <a type="button" class="btn btn-info save" onclick="return validateField(this, '#schet-doc_type', 1, 30, false, 1)"><span class="glyphicon glyphicon-floppy-disk"></span> Сохранить</a>
        <button type="button" class="btn btn-info" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Отмена</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="schetModalEmail" tabindex="-1" role="dialog" aria-labelledby="schetModalLabelEmail">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="schetModalLabelEmail">Отправить e-mail</h4>
      </div>
      <div class="modal-body">
        <form class="row">
          <div class="col-lg-6 col-md-6 col-sm-12">
            <span class="glyphicon glyphicon-print"></span> Выберите документы для печати

          </div>
          <div class="col-lg-6 col-md-6 col-sm-12">
            <span class="glyphicon glyphicon-print"></span> Кому
            
            <div class="radio">
              <label><input name="optradio" type="radio" id="optradio" data-toggle="collapse" data-target=".collapseOne.in" checked>
                 <span class="glyphicon glyphicon-user"></span> Себе <?=Yii::$app->user->identity->email?></label>
            </div>
            <div class="radio">
              <label><input name="optradio" type="radio" id="optradio2" data-toggle="collapse" data-target=".collapseOne:not(.in)"> Ввести другой e-mail</label>
            </div>


            <div class="form-group required collapseOne panel-collapse collapse">
              <input id="schet-email" class="form-control" type="text">
              <div class="help-block"></div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer backTo">
        <a type="button" class="btn btn-info email" onclick="return newValidateField(this, '#schet-email', 1, 30, true, 1)" style="float:left"><span class="glyphicon glyphicon-print"></span> Отправить</a>

        <button type="button" class="btn btn-info" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Отмена</button>
      </div>
    </div>
  </div>
</div>