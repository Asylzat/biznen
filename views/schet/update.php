<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Schet */

$this->title = 'Счет редактировать - ' . $model->id;

?>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userheader.php'); ?>
<div class="schet-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'document' => $document,
    ]) ?>

</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
