<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Подпись и печать ';



echo \Yii::$app->view->renderFile('@app/views/site/userheader.php');
?>
<div class="user-update">

    <h3> <?= Html::encode($this->title) ?></h3>

    <?= $this->render('_file', [
        'model' => $model,
        'resultStr' => $resultStr,
    ]) ?>

</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
