<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('/js/jquery.Jcrop.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/jquery.SimpleCropper.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

//$this->registerJsFile('/js/component.js');


$this->registerCssFile('/css/style.css');
$this->registerCssFile('/css/style-example.css');
$this->registerCssFile('/css/jquery.Jcrop.css');

$this->registerJs("
	$('.cropme').simpleCropper();
");
$this->registerCss("
.imageBlock {
background: #FFF;
padding: 23px;
margin-bottom: 25px;
min-height: 317px;
}

.loadImage {
position: relative;
top: 34px;
right: -17px;
}

.imageLoadText {
  color:grey;
  margin-bottom:10px;
}

  ");

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>





<?php if ($resultStr != '') echo "<h4>" .$resultStr. "</h4>"; ?>



<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-12">
    <div class="imageBlock">
      <div class="imageLoadText">Логотип (54X27 мм)</div>
      <div class="cropme cropme1" style="width: 266px; height: 158px;">
        <?php 
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/upload/' . Yii::$app->user->getId() . 'img1.png')) echo '<img src="/upload/' . Yii::$app->user->getId() . 'img1.png" width="216" height="108" />';
        ?>
        <a class="loadImage linkBtn"><span class="icomoon icomoon-cloud-download"></span> <span class="icTextBtn">Загрузить</span></a>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12">
    <div class="imageBlock">
      <div class="imageLoadText">Печать (40X40 мм)</div>
      <div class="cropme cropme2" style="width: 210px; height: 210px;">
        <?php 
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/upload/' . Yii::$app->user->getId() . 'img2.png')) echo '<img src="/upload/' . Yii::$app->user->getId() . 'img2.png" width="160" height="160" />';
        ?>
          <a class="loadImage linkBtn"><span class="icomoon icomoon-cloud-download"></span> <span class="icTextBtn">Загрузить</span></a>
      </div>
    </div>
  </div>

  <div class="col-lg-4 col-md-4 col-sm-12">
    <div class="imageBlock">
      <div class="imageLoadText">Подпись руководителя (34X16 мм)</div>
        <div class="cropme cropme3" style="width: 186px; height: 114px;">
          <?php 
          if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/upload/' . Yii::$app->user->getId() . 'img3.png')) echo '<img src="/upload/' . Yii::$app->user->getId() . 'img3.png" width="136" height="64" />';
          ?>
            <a class="loadImage linkBtn"><span class="icomoon icomoon-cloud-download"></span> <span class="icTextBtn">Загрузить</span></a>
        </div>
    </div>
  </div>
  <div class="col-lg-4 col-md-4 col-sm-12">
    <div class="imageBlock">
      <div class="imageLoadText">Подпись бухгалтера (34X16 мм)</div>
      <div class="cropme cropme4" style="width: 186px; height: 114px;">
        <?php 
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/upload/' . Yii::$app->user->getId() . 'img4.png')) echo '<img src="/upload/' . Yii::$app->user->getId() . 'img4.png" width="136" height="64" />';
        ?>
          <a class="loadImage linkBtn"><span class="icomoon icomoon-cloud-download"></span> <span class="icTextBtn">Загрузить</span></a>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-md-4 col-sm-12">
    <div class="imageBlock">
      <div class="imageLoadText">Подпись ИП (34X16 мм)</div>
        <div class="cropme cropme5" style="width: 186px; height: 114px;">
          <?php 
          if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/upload/' . Yii::$app->user->getId() . 'img5.png')) echo '<img src="/upload/' . Yii::$app->user->getId() . 'img5.png" width="136" height="64" />';
          ?>
            <a class="loadImage linkBtn"><span class="icomoon icomoon-cloud-download"></span> <span class="icTextBtn">Загрузить</span></a>
        </div>
    </div>
  </div>

  <div class="clear"></div>
</div>











<?= $form->field($model, 'image1')->hiddenInput(['value' => 'hidden value'])->label(false) ?>
<?= $form->field($model, 'image2')->hiddenInput(['value' => 'hidden value'])->label(false) ?>
<?= $form->field($model, 'image3')->hiddenInput(['value' => 'hidden value'])->label(false) ?>
<?= $form->field($model, 'image4')->hiddenInput(['value' => 'hidden value'])->label(false) ?>
<?= $form->field($model, 'image5')->hiddenInput(['value' => 'hidden value'])->label(false) ?>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
