<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if ($status == 'personal') { ?>

        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>


        <?= $form->field($model, 'name_contr')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'kpp')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'ogrn')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'gen_director')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'ur_adress')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'fak_adress')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?php } else if ($status == 'email') { ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php } else if ($status == 'password') { ?>

        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'passwordrepeat')->passwordInput() ?>

    <?php } else { ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ur_adress')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fak_adress')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'money')->textInput() ?>

    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
