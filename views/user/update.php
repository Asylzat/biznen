<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Изменить профиль: ' . $model->id;

if (!isset($status)) $status = '';

echo \Yii::$app->view->renderFile('@app/views/site/userheader.php');
?>
<div class="user-update">

    <h1><span class="glyphicon glyphicon-wrench"></span> <?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'status' => $status,
    ]) ?>

</div>

<?php echo \Yii::$app->view->renderFile('@app/views/site/userfooter.php'); ?>
