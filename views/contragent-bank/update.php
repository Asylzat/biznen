<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ContagentBank */

$this->title = 'Update Contagent Bank: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Contagent Banks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contagent-bank-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
