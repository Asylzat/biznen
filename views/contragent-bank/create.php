<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ContagentBank */

$this->title = 'Create Contagent Bank';
$this->params['breadcrumbs'][] = ['label' => 'Contagent Banks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contagent-bank-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
