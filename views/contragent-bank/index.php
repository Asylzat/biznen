<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContragentBankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contagent Banks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contagent-bank-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Contagent Bank', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'contragent_id',
            'bik',
            'name',
            'kor_schet',
            // 'ras_schet',
            // 'number',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
