<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ContragentBankSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contagent-bank-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'contragent_id') ?>

    <?= $form->field($model, 'bik') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'kor_schet') ?>

    <?php // echo $form->field($model, 'ras_schet') ?>

    <?php // echo $form->field($model, 'number') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
