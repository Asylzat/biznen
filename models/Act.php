<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "act".
 *
 * @property integer $id
 * @property integer $id_document
 * @property string $date
 */
class Act extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'act';
    }

    public function rules()
    {
        return [
            [['id_document', 'date', 'count'], 'required'],
            [['id_document', 'count'], 'integer'],
            [['sum', 'idnum'], 'string', 'max' => 100],
            [['date', 'prize'], 'string', 'max' => 300],
            [['service'], 'string', 'max' => 1000],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_document' => 'Id Документа',
            'date' => 'Дата',
            'sum' => 'Сумма',
            'service' => 'Наименование услуги / работы',
            'prize' => 'Цена',
            'count' => 'Кол-во',
            'idnum' => 'Счет №',
        ];
    }

    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'id_document']);
    }

    public function getServices()
    {
        return $this->hasMany(Service::className(), ['act_id' => 'id'])->orderBy('number');
    }
}
