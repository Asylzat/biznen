<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "document".
 *
 * @property integer $id
 * @property string $identify
 * @property string $company
 * @property string $date
 */
class Document extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prodaves_id', 'pokupatel_id', 'pechatforma', 'nds', 'currency', 'pay_for', 'byDogovor'], 'required', 'message'=>'Необходимо заполнить'],
            [['prodaves_id', 'pokupatel_id'], 'integer', 'message'=>'Необходимо ввести строку'],
            [['pay_for', 'byDogovor'], 'string', 'max'=>300, 'min'=>2, 'tooLong' => 'Необходимо ввести меньше символов', 'tooShort'=>'Необходимо ввести больше символов'],
            [['nds', 'currency', 'pechatforma'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prodaves_id' => 'Укажите продавца',
            'pokupatel_id' => 'Укажите покупателя',
            'pay_for' => 'Оплата за ...',
            'byDogovor' => '...по Договору (номер, дата)',
            'pechatforma' => 'Печатная форма счета',
            'nds' => 'НДС',
            'currency' => 'Валюта',
        ];
    }

    public function getSchet()
    {
        return $this->hasOne(Schet::className(), ['id_document' => 'id']);
    }

    public function getTorg12()
    {
        return $this->hasOne(Torg12::className(), ['id_document' => 'id']);
    }

    public function getAct()
    {
        return $this->hasOne(Act::className(), ['id_document' => 'id']);
    }

    public function getSchetFaktura()
    {
        return $this->hasOne(SchetFaktura::className(), ['id_document' => 'id']);
    }

    public function getPlatejPoruch()
    {
        return $this->hasOne(PlatejPoruch::className(), ['id_document' => 'id']);
    }

    public function getUpd()
    {
        return $this->hasOne(Upd::className(), ['id_document' => 'id']);
    }

    public function getProdaves()
    {
        return $this->hasOne(Contragent::className(), ['id' => 'prodaves_id']);
    }

    public function getPokupatel()
    {
        return $this->hasOne(Contragent::className(), ['id' => 'pokupatel_id']);
    }
}
