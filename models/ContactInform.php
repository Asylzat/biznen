<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact_inform".
 *
 * @property integer $id
 * @property string $fio
 * @property string $email
 * @property string $inform
 * @property integer $contragent_id
 * @property integer $number
 */
class ContactInform extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_inform';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'email', 'inform', 'contragent_id', 'number'], 'required'],
            [['contragent_id', 'number'], 'integer'],
            [['fio', 'email'], 'string', 'max' => 300],
            [['inform'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО сотрудника',
            'email' => 'E-Mail',
            'inform' => 'Дополнительная информация',
            'contragent_id' => 'Contragent ID',
            'number' => 'Number',
        ];
    }

    public static function serviceData($Post, $id, $field){
                    
        if (isset($Post["ServiceDelete2"])){
            $ServiceDelete = explode(';', $Post["ServiceDelete2"]);
            foreach($ServiceDelete as $current){
              if ($current != '') {
                $service = new ContactInform();
                $service = $service->findOne($current);
                if ($service != null) $service->delete();
              }
            }
        }
        
        foreach($Post["ContactInform"] as $current){
          $service = new ContactInform();
          if (isset($current["serviceId"]) && $current["serviceId"] != '') $service = $service->findOne($current["serviceId"]);
          else $service->$field = $id;
          
          if ($service != null){
            $service->fio = $current["fio"];
            $service->email = $current["email"];
            $service->inform = $current["inform"];
            $service->number = $current["number"];
            
            $service->save(false);                
          }
        }
    }
}
