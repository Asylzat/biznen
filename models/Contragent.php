<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contragent".
 *
 * @property integer $id
 * @property string $name_contr
 * @property string $name_contr_full
 * @property string $svidet_reg_ip
 */
class Contragent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contragent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_contr', 'name_contr_full', 'svidet_reg_ip', 'inn', 'kpp', 'ogrn',
            'phone', 'name_contr', 'gen_director', 'ur_adress', 'fak_adress', 'name_contr_full',
            'svidet_reg_ip', 'okpo', 'in_predprinimatel', 'nds', 'currency'], 'required', 'message' => 'Необходимо заполнить'],
            
            [['inn'], 'string', 'max'=>10, 'min'=>10, 'tooLong' => 'Необходимо ввести 10 символов', 'tooShort'=>'Необходимо ввести 10 символов'],
            [['kpp'], 'string', 'max'=>9, 'min'=>9, 'tooLong' => 'Необходимо ввести 9 символов', 'tooShort'=>'Необходимо ввести 9 символов'],
            [['ogrn'], 'string', 'max'=>13, 'min'=>13, 'tooLong' => 'Необходимо ввести 13 символов', 'tooShort'=>'Необходимо ввести 13 символов'],

            [['phone', 'fax', 'group', 'nds', 'currency'], 'string', 'max' => 100, 'tooLong' => 'Необходимо ввести меньше < 100 символов'],
            [['name_contr', 'gen_director', 'name_contr_full', 'svidet_reg_ip', 'okpo',
            'glav_buhgalter', 'in_predprinimatel', 'uslov_name_pokup'], 'string', 'max' => 300, 'tooLong' => 'Необходимо ввести меньше < 300 символов'],
            [['ur_adress', 'fak_adress', 'poch_adress'], 'string', 'max' => 1000, 'tooLong' => 'Необходимо ввести меньше < 1000 символов'],

            [['pasport', 'preambula1_2', 'preambula2_2'], 'string', 'max' => 3000, 'tooLong' => 'Необходимо ввести меньше < 3000 символов'],
            [['dop_inform', 'about', 'zametka', 'dop_code' ], 'string', 'max' => 5000, 'tooLong' => 'Необходимо ввести меньше < 5000 символов']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_contr' => 'Наименование контрагента',
            'name_contr_full' => 'Полное наименование контрагента',
            'svidet_reg_ip' => 'Свидетельство о регистрации ИП',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'ogrn' => 'ОГРН',
            'okpo' => 'ОКПО',
            'gen_director' => 'Генеральный директор (Ген. директор)',
            'pasport' => 'Удостоверение личности физлица',
            'phone' => 'Телефон (тел.)',
            'glav_buhgalter' => 'Главный бухгалтер (Гл. бухгалтер)',
            'in_predprinimatel' => 'Индивидуальный Предприниматель (ИП)',
            'preambula1_2' => 'Преамбула для Актов и Договоров (часть 1/2)',
            'preambula2_2' => 'Преамбула (часть 2/2)',

            'fax' => 'Факс (факс)',
            'group' => 'Группа',
            'uslov_name_pokup' => 'Условное наименование покупателя',
            'nds' => 'НДС',
            'currency' => 'Валюта',
            'dop_inform' => 'Дополнительная информация',
            'about' => 'О компании',
            'zametka' => 'Заметка',
            'dop_code' => 'Дополнительные коды',
            'ur_adress' => 'Юридический адрес',
            'fak_adress' => 'Фактический адрес',
            'poch_adress' => 'Почтовый адрес',

            'innNameSearch' => 'Поиск контрагента',

        ];
    }

    public function getBanks()
    {
        return $this->hasMany(ContagentBank::className(), ['contragent_id' => 'id'])->orderBy('number');
    }

    public function getInforms()
    {
        return $this->hasMany(ContactInform::className(), ['contragent_id' => 'id'])->orderBy('number');
    }

    public function getProdaves()
    {
        return $this->hasMany(Document::className(), ['prodaves_id' => 'id']);
    }

    public function getPokupatel()
    {
        return $this->hasMany(Document::className(), ['pokupatel_id' => 'id']);
    }

}
