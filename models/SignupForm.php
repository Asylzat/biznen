<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class SignupForm extends Model
{
    public $username;
    public $ur_adress;
    public $fak_adress;
    public $password;
    public $passwordrepeat;
    public $email;

    public $phone;
    public $name_contr;
    public $inn;
    public $kpp;
    public $ogrn;
    public $gen_director;

    private $_user = false;

    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password', 'passwordrepeat', 'email', 'name_contr', 'gen_director', 'ur_adress', 'fak_adress',
            'kpp', 'ogrn', 'phone'], 'required', 'message' => 'Необходимо заполнить'],
            [['username', 'password', 'passwordrepeat', 'email'], 'trim'],
            [['email'], 'email', 'message' => 'email указан не правильно'],
            //[['money'], 'safe'],
            [['username'], 'string', 'max'=>10, 'min'=>10, 'tooLong' => 'Необходимо ввести 10 символов', 'tooShort'=>'Необходимо ввести 10 символов'],
            [['kpp'], 'string', 'max'=>9, 'min'=>9, 'tooLong' => 'Необходимо ввести 9 символов', 'tooShort'=>'Необходимо ввести 9 символов'],
            [['ogrn'], 'string', 'max'=>13, 'min'=>13, 'tooLong' => 'Необходимо ввести 13 символов', 'tooShort'=>'Необходимо ввести 13 символов'],
            [['password','passwordrepeat'], 'string', 'max'=>600, 'min'=>4, 'tooLong' => 'too long', 'tooShort'=>'tooShort'],
            // password is validated by validatePassword()
            ['password', 'passwordRepeat'],

            [['phone'], 'string', 'max' => 100, 'tooLong' => 'Необходимо ввести меньше < 100 символов'],
            [['name_contr', 'gen_director'], 'string', 'max' => 300, 'tooLong' => 'Необходимо ввести меньше < 300 символов'],
            [['ur_adress', 'fak_adress' ], 'string', 'max' => 1000, 'tooLong' => 'Необходимо ввести меньше < 1000 символов'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин(ИНН)',
            'password' => 'Пароль',
            'passwordrepeat' => 'Повторить пароль',
            'email' => 'Емайл',
            'money' => 'Деньги',

            'name_contr' => 'Наименование контрагента',
            'kpp' => 'КПП',
            'ogrn' => 'ОГРН',
            'gen_director' => 'Генеральный директор (Ген. директор)',
            'phone' => 'Телефон (тел.)',

            'ur_adress' => 'Юридический адрес',
            'fak_adress' => 'Фактический адрес',
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function passwordRepeat()
    {
            if ($this->password != $this->passwordrepeat) {
                $this->addError('passwordrepeat', 'Пароль не совпадает.');
                return false;
            }
            return true;
    }

    public function uniqUsername(){
        if (User::find()->select(['username'])->where(['username' => $this->username])->count() > 0) {
            $this->addError('username', 'Имя пользователя должно быть уникально.');
            return false; 
        }
        return true;
    }

    public function uniqEmail(){
        if (User::find()->select(['email'])->where(['email' => $this->email])->count() > 0) {
            $this->addError('email', 'Уже есть пользователь с такой почтой должно быть уникально.');
            return false; 
        }
        return true;
    }


    public function randomString($length = 6) {
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

}
