<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contagent_bank".
 *
 * @property integer $id
 * @property integer $contragent_id
 * @property string $bik
 * @property string $name
 * @property string $kor_schet
 * @property string $ras_schet
 * @property integer $number
 */
class ContagentBank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contagent_bank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contragent_id', 'bik', 'name', 'kor_schet', 'ras_schet', 'number'], 'required'],
            [['contragent_id', 'number'], 'integer'],
            [['bik'], 'string', 'max' => 100],
            [['name', 'kor_schet', 'ras_schet'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contragent_id' => 'Contragent ID',
            'bik' => 'БИК',
            'name' => 'Наименование Банка и город',
            'kor_schet' => 'Кор. счет (к/с)',
            'ras_schet' => 'Расчетный счет (р/с)',
            'number' => 'Number',
        ];
    }

    public static function serviceData($Post, $id, $field){
                    
        if (isset($Post["ServiceDelete"])){
            $ServiceDelete = explode(';', $Post["ServiceDelete"]);
            foreach($ServiceDelete as $current){
              if ($current != '') {
                $service = new ContagentBank();
                $service = $service->findOne($current);
                if ($service != null) $service->delete();
              }
            }
        }
        
        foreach($Post["ContagentBank"] as $current){
          $service = new ContagentBank();
          if (isset($current["serviceId"]) && $current["serviceId"] != '') $service = $service->findOne($current["serviceId"]);
          else $service->$field = $id;
          
          if ($service != null){
            $service->bik = $current["bik"];
            $service->name = $current["name"];
            $service->kor_schet = $current["kor_schet"];
            $service->number = $current["number"];
            $service->ras_schet = $current["ras_schet"];
            
            $service->save(false);                
          }
        }
    }
}
