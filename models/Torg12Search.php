<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Torg12;

/**
 * Torg12Search represents the model behind the search form about `app\models\Torg12`.
 */
class Torg12Search extends Torg12
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_document'], 'integer'],
            [['date', 'sum', 'prize', 'delivsend', 'delivget', 'sendokpo', 'getokpo', 'structdevide', 'transnaklad', 
            'date2', 'vidoperas', 'tovar_naklad', 'priloj', 'service', 'count'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Torg12::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_document' => $this->id_document,
        ]);

        if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['author'])) 
            $query->andFilterWhere(['createdBy' => Yii::$app->user->getId()]);

        $query->andFilterWhere(['like', 'date', $this->date]);

        return $dataProvider;
    }
}
