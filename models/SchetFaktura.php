<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schet_faktura".
 *
 * @property integer $id
 * @property string $date
 */
class SchetFaktura extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schet_faktura';
    }


    public function rules()
    {
        return [
            [['id_document', 'date', 'count'], 'required'],
            [['id_document', 'count'], 'integer'],
            [['sum', 'idnum'], 'string', 'max' => 100],
            [['date', 'prize', 'delivsend', 'delivget'], 'string', 'max' => 300],
            [['service'], 'string', 'max' => 1000],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_document' => 'Id Документа',
            'date' => 'Дата',
            'sum' => 'Сумма',
            'service' => 'Наименование услуги / работы',
            'prize' => 'Цена',
            'count' => 'Кол-во',

            'delivsend' => 'Грузоотправитель',
            'delivget' => 'Грузополучатель',
            'idnum' => 'Счет №',
        ];
    }

    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'id_document']);
    }

    public function getServices()
    {
        return $this->hasMany(Service::className(), ['schet_faktura_id' => 'id'])->orderBy('number');
    }
}
