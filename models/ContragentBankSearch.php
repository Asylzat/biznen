<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ContagentBank;

/**
 * ContragentBankSearch represents the model behind the search form about `app\models\ContagentBank`.
 */
class ContragentBankSearch extends ContagentBank
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contragent_id', 'number'], 'integer'],
            [['bik', 'name', 'kor_schet', 'ras_schet'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContagentBank::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contragent_id' => $this->contragent_id,
            'number' => $this->number,
        ]);

        $query->andFilterWhere(['like', 'bik', $this->bik])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'kor_schet', $this->kor_schet])
            ->andFilterWhere(['like', 'ras_schet', $this->ras_schet]);

        return $dataProvider;
    }
}
