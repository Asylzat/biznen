<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "platej_poruch".
 *
 * @property integer $id
 * @property string $date
 */
class PlatejPoruch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'platej_poruch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'required'],
            [['sum', 'idnum'], 'string', 'max' => 100],
            [['date'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_document' => 'Id Документа',
            'date' => 'Дата',
            'sum' => 'Сумма',

            'idnum' => 'Счет №',
        ];
    }

    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'id_document']);
    }

    public function getServices()
    {
        return $this->hasMany(Service::className(), ['platej_poruch_id' => 'id'])->orderBy('number');
    }
}
