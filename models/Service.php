<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property integer $schet_id
 * @property integer $torg12_id
 * @property integer $act_id
 * @property integer $number
 * @property string $title
 * @property string $prize
 * @property string $count
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['schet_id', 'torg12_id', 'act_id', 'number', 'title', 'prize', 'count'], 'required'],
            [['schet_id', 'torg12_id', 'act_id', 'upd_id', 'platej_poruch_id', 'number'], 'integer'],
            [['title'], 'string', 'max' => 300],
            [['prize', 'count', 'sum', 'code', 'code_name', 'nds'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'schet_id' => 'Schet ID',
            'torg12_id' => 'Torg12 ID',
            'act_id' => 'Act ID',
            'number' => 'Number',
            'title' => 'Title',
            'prize' => 'Prize',
            'count' => 'Count',
            'sum' => 'sum',
            'code' => 'code',
            'code_name' => 'code_name',
            'nds' => 'nds',
        ];
    }

    public static function serviceData($Post, $id, $field){
                    
        if (isset($Post["ServiceDelete"])){
            $ServiceDelete = explode(';', $Post["ServiceDelete"]);
            foreach($ServiceDelete as $current){
              if ($current != '') {
                $service = new Service();
                $service = $service->findOne($current);
                if ($service != null) $service->delete();
              }
            }
        }
        
        foreach($Post["Service"] as $current){
          $service = new Service();
          if (isset($current["serviceId"]) && $current["serviceId"] != '') $service = $service->findOne($current["serviceId"]);
          else $service->$field = $id;
          
          if ($service != null){
            $service->title = $current["title"];
            $service->prize = $current["prize"];
            $service->count = $current["count"];
            $service->number = $current["number"];
            $service->sum = $current["sum"];
            $service->code = $current["code"];
            $service->code_name = $current["code_name"];
            $service->nds = $current["nds"];
            
            $service->save(false);                
          }
        }
    }
}
