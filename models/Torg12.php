<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "torg_12".
 *
 * @property integer $id
 * @property integer $id_document
 * @property string $date
 */
class Torg12 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'torg_12';
    }

    public function rules()
    {
        return [
            [['id_document', 'date', 'count'], 'required', 'message'=>'Необходимо заполнить'],
            [['id_document', 'count'], 'integer'],
            [['sum', 'idnum'], 'string', 'max' => 100],
            [['date', 'prize', 'delivsend', 'delivget', 'sendokpo', 'getokpo', 'structdevide', 'transnaklad', 
            'date2', 'vidoperas', 'tovar_naklad', 'priloj', 'okpdo'], 'string', 'max' => 300],
            [['service'], 'string', 'max' => 1000],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_document' => 'Id Документа',
            'date' => 'Дата',
            'sum' => 'Сумма',
            'service' => 'Наименование услуги / работы',
            'prize' => 'Цена',
            'count' => 'Кол-во',

            'delivsend' => 'Грузоотправитель',
            'delivget' => 'Грузополучатель',
            'sendokpo' => 'ОКПО',
            'getokpo' => 'ОКПО',
            'okpdo' => 'ОКПД',
            'structdevide' => 'Структурное подразделение',
            'transnaklad' => 'Транспортная накл. №',
            'date2' => 'Дата',
            'vidoperas' => 'Вид операции',
            'tovar_naklad' => 'Товар. наклад. имеет приложения на листах',
            'priloj' => 'Приложение (паспорта, сертификаты и т.п.)',
            'idnum' => 'Счет №',
        ];
    }

    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'id_document']);
    }

    public function getServices()
    {
        return $this->hasMany(Service::className(), ['torg12_id' => 'id'])->orderBy('number');
    }

}
