<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contragent;

/**
 * ContragentSearch represents the model behind the search form about `app\models\Contragent`.
 */
class ContragentSearch extends Contragent
{

    public $innNameSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name_contr', 'name_contr_full', 'svidet_reg_ip', 'innNameSearch', 'inn'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contragent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['author'])) 
            $query->andFilterWhere(['createdBy' => Yii::$app->user->getId()]);


        $query->andFilterWhere(['like', 'name_contr', $this->name_contr])
            ->andFilterWhere(['like', 'name_contr_full', $this->name_contr_full])
            ->andFilterWhere(['like', 'svidet_reg_ip', $this->svidet_reg_ip])
            //->andFilterWhere(['like', 'inn', $this->innNameSearch])
            ->andFilterWhere([
                'or',
                ['like', 'name_contr', $this->innNameSearch],
                ['like', 'inn', $this->innNameSearch],
            ]);

        return $dataProvider;
    }
}
