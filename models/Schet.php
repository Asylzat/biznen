<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schet".
 *
 * @property integer $id
 * @property integer $id_document
 * @property string $date
 */
class Schet extends \yii\db\ActiveRecord
{
    public $pathPHPExcel;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schet';
    }

    public function rules()
    {
        return [
            [['id_document', 'date'], 'required', 'message'=>'Необходимо заполнить'],
            [['id_document'], 'integer', 'message'=>'Необходимо ввести строку'],
            [['date'], 'string', 'max' => 300],
            [['sum', 'idnum'], 'string', 'max' => 100],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_document' => 'Id Документа',
            'date' => 'Дата',
            'idnum' => 'Счет №',
        ];
    }

    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'id_document']);
    }

    public function getServices()
    {
        return $this->hasMany(Service::className(), ['schet_id' => 'id'])->orderBy('number');
    }

    public function saveExcelFile($objPHPExcel, $file){
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: ' . $file . ';filename="01simple.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function savePDFFile($objPHPExcel, $file, $landScape = false){
        $rendererName = \PHPExcel_Settings::PDF_RENDERER_MPDF;
        $rendererLibrary = 'mpdf60';

        $rendererLibraryPath = $this->pathPHPExcel .'/mpdf60';

        if (!\PHPExcel_Settings::setPdfRenderer(
          $rendererName,
          $rendererLibraryPath
         )) {
         die(
          'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
          '<br />' .
          'at the top of this script as appropriate for your directory structure'
         );
        }

        // Redirect output to a client’s web browser (PDF)
        header('Content-Type: application/pdf');
        header('Content-Disposition: ' . $file . ';filename="01simple.pdf"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');

        if ($landScape) $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        $objWriter->save('php://output');
        exit;
    }

    public function style(){
        $style = array(
            'clear' => array(
                'borders' => array(
                    'outline' => array(
                        'style' => 0,//\PHPExcel_Style_Border::BORDER_THICK,
                        'color' => array('argb' => 'DDDDDD'),
                    ),
                ),
            ),

            'borderTopNone' => array(
                'borders' => array(
                    'top' => array(
                        'style' => 10,//\PHPExcel_Style_Border::BORDER_THICK,
                        'color' => array('argb' => \PHPExcel_Style_Color::COLOR_RED),
                    ),
                ),
            ),

            'borderBottomNone' => array(
                'borders' => array(
                    'bottom' => array(
                        'style' => 10,//\PHPExcel_Style_Border::BORDER_THICK,
                        'color' => array('argb' => 'DDDDDD'),
                    ),
                ),
            ),

            'fontNormal' => array(
                'font' => array(
                  'bold' => false,
                ),
            ),

            'fontRight' => array(
                'alignment' => array(
                  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
            ),

            'fontLeft' => array(
                'alignment' => array(
                  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
            )
        );
        return $style;
    }


    public function sendEmail($objPHPExcel, $email){
        $contact = new \app\models\SendMail();
        $contact->name = 'Biznen';
        $contact->email = Yii::$app->params['adminEmail'];
        $contact->subject = 'Вы создали учетную запись на Biznen.com!';
        $contact->body = \Yii::$app->view->renderFile('@app/views/user/_signupMail.php', [
            'password' => 'email', 
            ]);

          // get the content
        @ob_start();

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        $data = @ob_get_contents();
        @ob_end_clean();

        $contact->attachment = $data;

        $contact->contact($email);
    }
}
