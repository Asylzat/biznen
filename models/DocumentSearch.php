<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Document;

/**
 * DocumentSearch represents the model behind the search form about `app\models\Document`.
 */
class DocumentSearch extends Document
{
    public $schetinn;
    public $prodaves;
    public $pokupatel;
    public $dateFrom;
    public $dateTo;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['prodaves_id', 'pokupatel_id', 'pay_for', 'byDogovor', 'pechatforma', 'nds', 'currency', 'schetinn', 'prodaves', 'pokupatel', 'dateFrom', 'dateTo', 'schet', 'torg12'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Document::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['schet', 'torg12', 'prodaves']);

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['author'])) 
            $query->andFilterWhere(['document.createdBy' => Yii::$app->user->getId()]);


        $query->andFilterWhere(['like', 'pay_for', $this->pay_for])
            ->andFilterWhere(['like', 'byDogovor', $this->byDogovor])
            ->andFilterWhere(['like', 'pechatforma', $this->pechatforma])
            /*->andFilterWhere([
                'or',
                ['schet.id' => $this->schetinn],
                ['like', 'schet.service', $this->schetinn],
            ])*/
            ;

        return $dataProvider;
    }
}
